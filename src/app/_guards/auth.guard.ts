import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';



@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    let customer = JSON.parse(localStorage.getItem("current-customer"));
    let token = JSON.parse(localStorage.getItem("token"));

    if (token) {
      return true;
    } else {
    	this.router.navigate(['/auth/sign-in'], {
        queryParams: {
          return: state.url
        }
      });

    	return false;
    }

  }
}
