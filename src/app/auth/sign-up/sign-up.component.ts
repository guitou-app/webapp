import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthService } from '../../_services/auth.service';


@Component({
  selector: 'nsi-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less']
})
export class SignUpComponent implements OnInit {

  signUpForm = new FormGroup({
    fullname: new FormControl('', Validators.required),
		phone: new FormControl('', Validators.required),
		email: new FormControl(
      this.activatedRoute.snapshot.queryParams['return'], 
      Validators.required
    ),
		password: new FormControl('', Validators.required),
		password_repeat: new FormControl('', Validators.required)
	});

  isLoading = false;
  error = null;

  constructor(
		private activatedRoute: ActivatedRoute,
    private router: Router,
    public authService: AuthService
  ) { }

  ngOnInit() {
  }

  signUp() {
    let user = this.signUpForm.value;
    this.error = null;
    this.isLoading = true;
    
		this.authService.signUp(user).pipe(first()).subscribe(
			(result:any) => {

				// localStorage.setItem("refresh-token", JSON.stringify(result.refresh_token));
				// localStorage.setItem("token", JSON.stringify(result.access_token));
				// localStorage.setItem("current-customer", JSON.stringify(result.user));

        // this.isLoading = false;
				// this.router.navigate([this.activatedRoute.snapshot.queryParams['return'] ? this.activatedRoute.snapshot.queryParams['return'] : '']);
        this.signIn();
			},
			(err) => {
        this.isLoading = false;
        this.error = "An error occured. Restart please.";
			}
		)
  }

	signIn() {
		this.router.navigate(['/auth/sign-in'], {
			queryParams: this.activatedRoute.snapshot.queryParams
		});
	}


}
