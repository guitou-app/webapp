import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { first } from 'rxjs/operators';

import { AuthService } from 'src/app/_services/auth.service';
import { User } from 'src/app/_models/user';

interface SignInResult {
	token: string;
	user: User;
}

@Component({
  selector: 'nsi-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent implements OnInit {

	signInForm = new FormGroup({
		email: new FormControl(
			this.activatedRoute.snapshot.queryParams['email'],
			Validators.required
		),
		password: new FormControl('', Validators.required)
	});

	isLoading: boolean = false;
  error = null;

  constructor(
		private activatedRoute: ActivatedRoute,
  	private router: Router,
  	public authService: AuthService
  ) { }

  ngOnInit() {

  }

  signIn() {
		this.isLoading = true;
		console.log(this.signInForm.value);
		this.authService.signIn(this.signInForm.value).pipe(first()).subscribe(
			(result:SignInResult) => {

				localStorage.setItem("token", JSON.stringify(result.token));
				localStorage.setItem("current-customer", JSON.stringify(result.user));
				
				this.isLoading = false;

				this.router.navigate([this.activatedRoute.snapshot.queryParams['return'] || 'projects']);
			},
			(error) => {
				console.log(error);
        this.isLoading = false;
        this.error = "An error occured. Restart please.";
			}
		)
	}

	signUp() {
		this.router.navigate(['/auth/sign-up'], {
			queryParams: this.activatedRoute.snapshot.queryParams
		});
	}

}
