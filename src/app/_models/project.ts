import { Collaborator } from "./collaborator";
import { Xorm } from "./xorm";

export interface Project {
  id: string;
  description: string;
  title: string;

  author: string;
  collaborators: Collaborator[];
  createdAt: Date;

  // primaryKey: {used: Array(0)}
  settings: {
    xorms: {
      pKeyOrigin: string;
      primary: string;
      geolocationPosition: { xorm: string; shouldTakeGeolocationPosition: boolean }[];
    };
    customURLId: Record<string, string>;
    defaultLanguage: string | null;
    languages?: string[];
  }

  xorms: (string | Xorm)[];
  translations: Record<string, {
    title: string;
    description: string;
  }>;
}

export type ProjectTranslations = Project['translations'];
