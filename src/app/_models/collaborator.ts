export interface Collaborator {
  id: string;
  name: string;
  email: string;
  status: string;
  role: string;
  invitationDate: Date;
  answerDate: Date;
}
