import * as _ from "lodash"

// RemoteXorm must be created
export interface Xorm {
  id: string;
  title: string;
  description: string;
  
  project: string;
  
  level: string;
  pKeyOrigin: string;

  author: string;
  createdAt: Date;
  
  // xorm: Record<string, any>; // Remote Section, _params: XormSectionParams, questions: Record<string, XormSection>>
  xorm: Record<string, XormSection>;
  translations: Record<string, {
    title: string;
    description: string;
    xorm: Record<string, XormSection>;
  }>;
  data: any[];
}

export type XormTranslations = Xorm['translations'];

export class XormSectionParams {
  key: string;
  title: string;
  description: string;
  hint: string;
  repeat?: boolean;

  constructor(key: string) {
    this.key = key;
    this.title = "Untitle section";
    this.description = "";
  }
}

export class XormSection {
  _params: XormSectionParams;
  questions: XormQuestion[];

  constructor(key: string) {
    this._params = new XormSectionParams(key);
    this.questions = [];
  }

  static fromJSON(key: string, json: Record<string, any>): XormSection {
    let section: XormSection = new XormSection(key);
    section._params = Object.assign(section._params, json["_params"]);
    section.questions = Object.keys(json['questions'] || {}).map(id => {
      const type = json['questions'][id]['type'];
      let question: XormQuestion;
      let data = json['questions'][id];
      
      switch (type) {
        case 'string':
          question = new XormQuestionString({...data, id});
          break;
        case 'text':
          question = new XormQuestionText({...data, id});
          break;
        case 'single-choice':
          question = new XormQuestionSingleChoice({...data, id});
          break;
        case 'multiple-choice':
          question = new XormQuestionMultipleChoice({...data, id});
          break;
        case 'yes-no':
          question = new XormQuestionYesNo({...data, id});
          break;
        case 'yes-no-dont':
          question = new XormQuestionYesNoDont({...data, id})
          break;
        case 'single-choice-select':
          question = new XormQuestionSingleChoiceSelect({...data, id});
          break;
        case 'datatable':
          question = new XormQuestionDatatable({...data, id});
          console.log('after XDatatable ', question);
          break;
        // case 'scale':
        //   question.type = 'scale';
        //   break;
        default:
          question = new XormQuestionString({...data, id});
          break;
      }

      return question;
    });
    return section;
  }

}

export abstract class XormQuestion {
  id: string;
  title: string;
  description: string;
  hint: string;
  type: string;

  constructor() {
    this.title = 'Untitled Question';
    this.description = '';
    this.hint = '';
  }
}

export class XormQuestionTitleDesc extends XormQuestion {
  constructor(source: Partial<XormQuestionTitleDesc>) {
    super();
    this.type = 'tidesc';
    this.title = 'Here ...';
    
    Object.assign(this, source);
  }
}

export class XormQuestionString extends XormQuestion {
  constructor(source: Partial<XormQuestionString>) {
    super();
    this.type = 'string';
  
    Object.assign(this, source);
  } 
}

export class XormQuestionText extends XormQuestion {
  constructor(source: Partial<XormQuestionText>) {
    super();
    Object.assign(this, source);
  }
}

export class XormQuestionNumber extends XormQuestion {
  constructor(source: Partial<XormQuestionNumber>) {
    super();
    Object.assign(this, source);
  }
}

export class XormQuestionDate extends XormQuestion {
  constructor(source: Partial<XormQuestionDate>) {
    super();
    Object.assign(this, source);
  }
}

export class XormQuestionTime extends XormQuestion {
  constructor(source: Partial<XormQuestionTime>) {
    super();
    Object.assign(this, source);
  }
}

export class XormQuestionOptional extends XormQuestion {}

interface IXormQuestionDatatableColumn {
  text: string;
  type?: string;
  choices?: {
    value: string
  }[];
}

interface IXormQuestionDatatableRow {
  text: string;
  header: boolean;
}

interface IXormQuestionDatatableSetting {
  cols: {
    customTypes: boolean;
  },
  rows: {
    none: boolean;
    displayedLinesDefault: number;
  }
}

export class XormQuestionDatatable extends XormQuestion {
  rows: IXormQuestionDatatableRow[];
  cols: IXormQuestionDatatableColumn[];
  settings: IXormQuestionDatatableSetting;

  constructor(source: Partial<XormQuestionDatatable>) {
    super();
    this.type = 'datatable';
    this.cols = [];
    this.rows = [];
    this.settings = {
      cols: {
        customTypes: false,
      },
      rows: {
        none: false,
        displayedLinesDefault: 1
      },
    };

    _.merge(this, source);
    for (let i=0; i < this.cols.length; i++) {
      if (!this.cols[i].hasOwnProperty('type')) {
        this.cols[i].type = 'Text';
      } else if (this.cols[i].type === 'Single Choice' && !this.cols[i].hasOwnProperty('choices')) {
        this.cols[i]['choices'] = [];
      }
    }
  } 

  addRow() {
    this.rows.push({
      text: 'Row ' + this.rows.length + 1,
      header: false
    });
  }

  removeRow(r: number) {
    this.rows.splice(r, 1);
  }

  addCol() {
    this.cols.push({
      text: 'Col ' + this.cols.length + 1,
      type: 'text'
    });
  }

  removeCol(c: number) {
    this.cols.splice(c, 1);
  }
}

export class XormQuestionOption {
  key: string;
  value: string;
  jumpTo: string;

  constructor(source: Partial<XormQuestionOption>) {
    Object.assign(this, source);

    if (!this.value) {
      this.value = this.key;
    }
  }

  static fromArray(options: XormQuestionOption[]) {
    return options.map(o => new XormQuestionOption({...o}));
  }
}

export class XormQuestionWithOptions extends XormQuestion {
  options: XormQuestionOption[] = [];
  otherOption: boolean = false;

  constructor() {
    super();

    this.addOption();
  }

  addOption() {
    this.options.push(new XormQuestionOption({
      key: `Option ${this.options.length + 1}`
    }));
    
    return this;
  }

  onRemoveOption(o: number) {
    if (this.options.length <= 1) {
      return;
    }
    this.options.splice(o, 1);

    return this;
  }

  setOptions(options: XormQuestionOption[]) {
    this.options = options;

    return this;
  }
}

export class XormQuestionSingleChoiceSelect extends XormQuestionWithOptions {
  addOptionAtEntry: boolean = false;

  constructor(source: Partial<XormQuestionSingleChoiceSelect>) {
    super();
    this.type = 'single-choice-select';

    Object.assign(this, source);
  }
}

export class XormQuestionSingleChoice extends XormQuestionWithOptions {
  constructor(source: Partial<XormQuestionSingleChoice>) {
    super();
    this.type = 'single-choice';

    Object.assign(this, source);
  }
}

export class XormQuestionYesNo extends XormQuestionWithOptions {
  constructor(source: Partial<XormQuestionSingleChoice>) {
    super();
    this.type = 'yes-no';

    Object.assign(this, source);
  }
}

export class XormQuestionYesNoDont extends XormQuestionWithOptions {
  constructor(source: Partial<XormQuestionSingleChoice>) {
    super();
    this.type = 'yes-no-dont';

    Object.assign(this, source);
  }
}

export class XormQuestionMultipleChoice extends XormQuestionWithOptions {
  constructor(source: Partial<XormQuestionMultipleChoice>) {
    super();
    this.type = 'multiple-choice';

    Object.assign(this, source);
  }
}
