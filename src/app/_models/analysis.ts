
export interface Analysis {
  id?: string;
  title: string;
  projectId: string;
  xormId: string;
  type: string;

  histogram?: Histogram
}


export interface Histogram {
  variable: string;
  data: number[];
  options: HistogramOptions
} 
interface HistogramOptions {
  bins: number;
}