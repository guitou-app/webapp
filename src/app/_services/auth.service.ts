import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()
export class AuthService {

	private url = environment.apiUrl + "/auth";

  constructor(
  	public http:HttpClient
	) { }

  signIn(credentials) {
    console.log('[signIn]', credentials, this.url);
  	return this.http.post(`${this.url}/signin`, credentials, httpOptions);
  }

  signUp(user) {
  	return this.http.post(this.url + '/signup', user, httpOptions);
  }

  signOut() {    
		return this.http.post(this.url + '/signout', {}); //, httpOptions);
  }

}
