import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  constructor(
    public http: HttpClient
  ) { }

  url(projectId) {
    return `${environment.apiUrl}/projects/${projectId}`;
  }

  create(projectId: string) {
    return this.http.post(`${this.url(projectId)}/xorms`, {});
  }

  fetchAll(projectId:string) {
    return this.http.get(`${this.url(projectId)}/xorms`);
  }

  fetchOne(projectId:string, xormId:string) {
    return this.http.get(`${this.url(projectId)}/xorms/${xormId}`);
  }

  // fetchOneNA(id:any) {
  //   console.log("fetchOneNA....");
  //   return this.http.get(`${this.url(projectId)}/xorms/${id}`);
  // }

  update(projectId:string, id:string, data) {
    return this.http.patch(`${this.url(projectId)}/xorms/${id}`, data);
  }

  remove(projectId:string, xormId:string) {
    return this.http.delete(`${this.url(projectId)}/xorms/${xormId}`);
  }

  fetchAllCollaborators(xormId) {
    return this.http.get(`${this.url}/a/xorms/${xormId}/collaborators`);
  }

  addCollaborator(xormId, collaborator) {
    return this.http.post(`${this.url}/a/xorms/${xormId}/collaborators`, collaborator);
  }

  removeCollaborator(xormId, id) {
    return this.http.delete(`${this.url}/a/xorms/${xormId}/collaborators/${id}`);
  }

  fetchAllCharts(xormId) {
    return this.http.get(`${this.url}/a/xorms/${xormId}/charts`);
  }

}
