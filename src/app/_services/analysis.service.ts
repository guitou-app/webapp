import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from './utils';
import { environment } from '../../environments/environment';
import { Analysis } from '../_models/analysis';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    public http: HttpClient
  ) { }

  initChart(projectId: string, xormId:string) {
    return this.http.post(`${environment.apiUrl}/analysis/${projectId}/init/${xormId}`, {});
  }

  fetchDashbaords(id:string): Observable<Analysis[]> {
    return this.http.get<Analysis[]>(`${environment.apiUrl}/analysis/${id}`);
  }

  addChart(id:string, data:Analysis): Observable<Analysis> {
    return this.http.post<Analysis>(`${environment.apiUrl}/analysis/${id}`, data, this.httpOptions)
      .pipe(
        catchError(handleError<Analysis>('addChart', data))
      );
  }

  deleteChart(projectId: string, chartId: string): Observable<boolean> {
    return this.http.delete<boolean>(`${environment.apiUrl}/analysis/${projectId}/${chartId}`)
      .pipe(
        catchError(handleError<boolean>('delete', false))
      );
  }
}
