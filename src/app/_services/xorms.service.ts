import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from 'file-saver';

import { BehaviorSubject, config, Observable } from 'rxjs';
import { catchError, first, map, retry, tap, timeout } from 'rxjs/operators'

import { environment } from '../../environments/environment';
import { Collaborator } from '../_models/collaborator';

import { ConfigService } from '../_helpers/config.service';
import { Socket } from 'ngx-socket-io';
import { Project, ProjectTranslations } from '../_models/project';
import { Xorm, XormTranslations } from '../_models/xorm';

type GeneratorBuildApkSEvent = string;
interface GeneratorBuildApkProcessingSEvent {
  projectId: string;
  message: string;
}
interface GeneratorBuildApkEndSEvent {
  projectId: string;
  result: boolean;
  path: string;
}

@Injectable({
  providedIn: 'root'
})
export class XormsService {
  buildApk = this.socket.fromEvent<GeneratorBuildApkSEvent>('generator.build-apk');
  buildApkProcessing = this.socket.fromEvent<GeneratorBuildApkProcessingSEvent>('generator.build-apk.processing');
  buildApkEnd = this.socket.fromEvent<GeneratorBuildApkEndSEvent>('generator.build-apk.end');

  private currentProjectSubject = new BehaviorSubject(null);
  currentProject$: Observable<Project> = this.currentProjectSubject.asObservable();

  constructor(
    public http: HttpClient,
    private socket: Socket
  ) { }

  fetchAll() {
    return this.http.get(`${environment.apiUrl}/projects`);
  }

  fetchOne(id:any): Observable<Project> {
    return this.http.get<Project>(`${environment.apiUrl}/projects/${id}`)
      .pipe(
        tap({
          next: project => this.currentProjectSubject.next(project)      
        })
      );
  }

  save(xorm) {
    return this.http.post(`${environment.apiUrl}/projects`, xorm);
  }

  update(projectId, xorm) {
    return this.http.put(`${environment.apiUrl}/projects/${projectId}`, xorm);
  }

  generate(projectId) {
    return this.http
    .get(
      `${environment.apiUrl}/generate/${projectId}`,
    ).pipe(timeout(180000));
  }

  getCustomURLId(projectId: string, xormId: string) {
    return this.http.get(`${environment.apiUrl}/projects/${projectId}/xorms/${xormId}/customURLId`);
  }

  updateProjectSettings(projectId: string, settings: Record<string, any>) {
    return this.http.patch(`${environment.apiUrl}/projects/${projectId}/settings`, settings);
  }

  updateXormSettings(projectId: string, xormId: string, settings: Record<string, any>) {
    return this.http.patch(`${environment.apiUrl}/projects/${projectId}/xorms/${xormId}/settings`, settings);
  }

  downloadApk(projectId: string, apkId: string = "") {
    return this.http
      .get(`${environment.apiUrl}/generate/${projectId}/apk?apkId=${apkId}`, { responseType: 'blob' })
  }

  delete(projectId) {
    return this.http.delete(`${environment.apiUrl}/projects/${projectId}`);
  }

  fetchCollaborators(projectId:string): Observable<Collaborator[]> {
    return this.http.get<Collaborator[]>(`${environment.apiUrl}/projects/${projectId}/collaborators`)
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  inviteCollaborator(projectId:string, email: string, role: string): Observable<Collaborator> {
    return this.http.post<Collaborator>(`${environment.apiUrl}/projects/${projectId}/invitation`, { email, role })
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  removeCollaborator(projectId: string, id: string) {
    return this.http.delete(`${environment.apiUrl}/projects/${projectId}/invitation/${id}`)
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  checkInvitation(projectId: string, invitationId:string) {
    return this.http.post(`${environment.apiUrl}/projects/${projectId}/invitation/${invitationId}/check`, {})
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  answerInvitation(projectId: string, invitationId: string, answer: string) {
    return this.http.post(`${environment.apiUrl}/projects/${projectId}/invitation/${invitationId}/answer`, { answer })
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  getXormById(projectId: string, xormId: string) {
    return this.http.get<Xorm>(`${environment.apiUrl}/projects/${projectId}/xorms/${xormId}`)
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  setProjectTranslation(projectId: string,
    body: {
      language: string,
      data: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations]
    }
  ) {
    return this.http.post(`${environment.apiUrl}/projects/${projectId}/translations`, body)
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

  setXormTranslation(projectId: string, xormId: string,
    body: {
      language: string,
      data: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations]
    }
  ) {
    return this.http.post(`${environment.apiUrl}/projects/${projectId}/xorms/${xormId}/translations`, body)
      .pipe(
        catchError(ConfigService.handleError)
      );
  }

}
