import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    public http: HttpClient
  ) { }

  fetchAll(id:string) {
    return this.http.get(`${environment.apiUrl}/a/xorms/${id}/data`);
  }

  fetchOne(id:string, projectId:string) {
    return this.http.get(`${environment.apiUrl}/a/xorms/${projectId}/data/${id}`);
  }

  save(id, data) {
    // return this.http.post(`${environment.apiUrl}/a/xorms/${id}/data`, data);
    // /data/w/projectId/xormId/ for web
    // /data/ for visitor.... 
    // /data/m/projectId/xormId/ for mobile
    return this.http.post(`${environment.apiUrl}/m/forms/${id}/data`, data);
  }

  update(id, data) {
    return this.http.put(`${environment.apiUrl}/a/xorms/${id}/data/${data._id}`, data);
  }

  delete(projectId, dataId) {
    return this.http.delete(`${environment.apiUrl}/a/xorms/${projectId}/data/${dataId}`);
  }

}
