import { Injectable } from '@angular/core';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class LocalUserService {
  
  private _user = JSON.parse(localStorage.getItem("current-customer"));
  private _token = JSON.parse(localStorage.getItem("token"));

  constructor() { 
    console.log(this._token, this._user);
  }

  get user(): User {
    return this._user;
  }

  get token(): string {
    return this._token;
  }
}
