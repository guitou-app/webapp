import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

import { Observable, Subject } from 'rxjs';

import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';
import { LocalUserService } from './local-user.service';

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {
  // Genrator - Build APK
  buildApkStart = this.socket.fromEvent('generator.build-apk');
  buildApkProcessing = this.socket.fromEvent<string>('generator.build-apk.processing');

  constructor(
    private socket: Socket,
    private localUserService: LocalUserService
  ) { }

  connectToServer() {}

  joinUserRoom() {
    this.socket.emit("join.room.user", this.localUserService.user.id);
  }

  joinProjectRoom(projectId: string) {
    this.socket.emit("join.room.project", projectId);
  }

  emitFormGeneration(params): Observable<any> {
    let observable = new Observable(observer => {
      console.log("emitFormGeneration... ", params);
      this.socket.emit("forms.generate", params, (data) => {
        console.log("forms.generate", data);

        observer.next(data);
      });
    });

    return observable;
  }

  emitNewXormCreation(projectId):Observable<any> {
    let params = {
      "projectId": projectId
    };

    let observable = new Observable(observer => {
      console.log("emitNewXorms... ", params);
      this.socket.emit("forms.new", params, (data) => {
        console.log("forms.new", data);

        observer.next(data);
      });
    });

    return observable;
  }

  emitSaveXorm(params):Observable<any> {
    let observable = new Observable(observer => {
      console.log("emitSaveXorm... ", params);
      this.socket.emit("forms.save.auto", params, (data) => {
        console.log("forms.save.auto", data);

        observer.next(data);
      });
    });

    return observable;
  }

  onFormGenerationMessages(): Observable<any> {
    let observable = new Observable(observer => {
      console.log("onFormGenerationMessages... ");
      this.socket.on("forms.generate.messages", (data) => {
        console.log("forms.generate.messages", data);

        observer.next(data);
      });
    });

    return observable;
  }

  joinDashbaords(xormId): Observable<any> {
    let params = {
      xormId: xormId
    };

    let observable = new Observable(observer => {
      console.log("forms.dashboard.join... ");
      this.socket.emit('forms.dashboard.join', params, (data) => {
        console.log("forms.dashboard.join", data);

        observer.next(data);
      });
    });

    return observable;
  }

  onNewDataSaved(): Observable<any> {
    let observable = new Observable(observer => {
      // console.log("onNewDataSaved... ");
      this.socket.on("new.data.saved", (data) => {
        // console.log("new.data.saved", data);

        observer.next(data);
      });
    });

    return observable;
  }

  emitCreateChartData(projectId, variable, chart):Observable<any> {
    const [xormId, question] = variable.split(" xxx ");
    let params = {
      projectId,
      xormId,
      question,
      chart
    };

    let observable = new Observable(observer => {
      console.log("emitCreateChartData... ", params);
      this.socket.emit("dashboard.create.chart.data", params, (data) => {
        console.log("dashboard.create.chart.data", data);

        observer.next(data);
      });
    });

    return observable;
  }

}

