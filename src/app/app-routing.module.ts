import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'x/:xormId',
  //   component: XormsDataNewComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    // { enableTracing: true }
    // { onSameUrlNavigation: 'reload' } 
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
