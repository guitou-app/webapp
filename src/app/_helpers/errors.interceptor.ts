import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../_services/auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    public authService: AuthService,
    public router: Router
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      console.log('errors.interceptors', err.status);
      if (err.status === 401) {
        this.authService.signOut();

        this.router.navigate(['/auth/sign-in']);
        return;
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
    }))
  }
}
