import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout.component';

import { XormsModule } from './xorms/xorms.module';
import { XormsDataNewComponent } from './xorms-data-new/xorms-data-new.component';

import { AuthGuard } from '../_guards/auth.guard';
import { JoinComponent } from './join/join.component';

const routes: Routes = [

  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'projects', pathMatch: 'full'},
      { path: 'projects', loadChildren: () => import('./xorms/xorms.module').then(x => x.XormsModule) },
      { path: 'join/:projectId/:invitationId', component: JoinComponent },
      { path: 'v/:projectId', component: XormsDataNewComponent },
    ],
    canActivate: [ AuthGuard ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
