import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDataNewComponent } from './xorms-data-new.component';

describe('XormsDataNewComponent', () => {
  let component: XormsDataNewComponent;
  let fixture: ComponentFixture<XormsDataNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsDataNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDataNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
