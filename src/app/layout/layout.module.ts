import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { LayoutRoutingModule } from './layout-routing.module';
import { XormsModule } from './xorms/xorms.module';

// import { XormsDataNewComponent } from './visitor/xorms-data-new/xorms-data-new.component';
import { XormsDataNewComponent } from './xorms-data-new/xorms-data-new.component';
import { LayoutComponent } from './layout.component';
import { StatsComponent } from './xorms/stats/stats.component';
import { JoinComponent } from './join/join.component';

@NgModule({
  declarations: [
    LayoutComponent,
    XormsDataNewComponent,
    JoinComponent
  ],
  imports: [
    CommonModule,

    SharedModule,
    LayoutRoutingModule,
    XormsModule
  ]
})
export class LayoutModule { }
