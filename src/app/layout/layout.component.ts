import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../_services/auth.service';

import * as $ from 'jquery';

import { first } from 'rxjs/operators';

import { SocketIoService } from '../_services/socket-io.service';
import { LocalUserService } from '../_services/local-user.service';

@Component({
  // encapsulation: ViewEncapsulation.None,
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.less']
})
export class LayoutComponent implements OnInit {

  isLoading = false;
  currentUser;

  constructor(
    public socketIoService:SocketIoService,
    private router: Router,
    private localUserService: LocalUserService,
    public authService: AuthService
  ) {
    this.socketIoService.connectToServer();
  }

  ngOnInit() {

    this.currentUser = this.localUserService.user;
    this.socketIoService.joinUserRoom();

    // this.socketIoService.onFormGenerationMessages().subscribe(
    //   (data) => {
    //     console.log(data.message);
    //   }
    // )
  }

  signOut() {
    console.log('Sign out');

    this.isLoading = true;
    // this.authService.signOut();
    // this.router.navigate(['']);
    // this.isLoading = false;

    console.log('Success Out');

		// console.log(user);
		this.authService.signOut().pipe(first()).subscribe(
			(result:any) => {
      	localStorage.removeItem('token');
      	localStorage.removeItem('refresh-token');
      	localStorage.removeItem('current-customer');

        console.log(localStorage.getItem("token"));
        this.isLoading = false;
				this.router.navigate(['/auth/sign-in']);
			},
			(error) => {
        this.isLoading = false;
        // this.error = "An error occured. Restart please.";
        console.log("An error occured. Restart please.");
				console.log(error);
			}
		)
  }

}
