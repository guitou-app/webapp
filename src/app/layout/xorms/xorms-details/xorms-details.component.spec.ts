import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDetailsComponent } from './xorms-details.component';

describe('XormsDetailsComponent', () => {
  let component: XormsDetailsComponent;
  let fixture: ComponentFixture<XormsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
