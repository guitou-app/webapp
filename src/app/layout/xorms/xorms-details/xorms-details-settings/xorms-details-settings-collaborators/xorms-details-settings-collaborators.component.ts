import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { result } from 'lodash';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Collaborator } from 'src/app/_models/collaborator';

import { XormsService } from '../../../../../_services/xorms.service';

@Component({
  selector: 'app-xorms-details-settings-collaborators',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './xorms-details-settings-collaborators.component.html',
  styleUrls: ['./xorms-details-settings-collaborators.component.less']
})
export class XormsDetailsSettingsCollaboratorsComponent implements OnInit {

  projectId: string;
  isLoading: boolean = false;
  collaborators: Collaborator[] = [];
  emailToInvite: string = '';

  constructor(
    private activatedRoute:ActivatedRoute,
    private xormsService: XormsService,
    private modalService: NzModalService,
    private messageService: NzMessageService
  ) { }

  ngOnInit(): void {
    this.projectId = this.activatedRoute.parent.snapshot.params['projectId'];
    console.log('XormsDetailsCollaborators...', this.projectId);

    this.fetchCollaborators();
  }

  private fetchCollaborators() {
    console.log('fetchCollaborators()');
    
    this.isLoading = true;

    this.xormsService.fetchCollaborators(this.projectId).subscribe(
      (result: Collaborator[]) => {
        console.log("Result... FORM ... ", result);
        this.isLoading = false;

        this.collaborators = result;
      }
    );
  }

  removeCollaborator(id: string) {
    console.log('removeCollaborator() ', id);
    
    this.isLoading = true;

    this.xormsService.removeCollaborator(this.projectId, id).subscribe(
      (result: Collaborator[]) => {
        console.log("Result... FORM ... ", result);
        this.isLoading = false;

        this.collaborators = this.collaborators.filter(c => c.id != id);

        this.messageService.create('success', 'Collaborator successfully removed');
      },
      (error) => {
        this.isLoading = false;

        this.messageService.create('error', 'Sorry, an error occurred');
      }
    );
  }

  startInvitationProcess() {
    const modal = this.modalService.create({
      nzTitle: "Invite a collaborator",
      nzContent: InviteCollaboratorComponent,
      nzFooter: null,
      nzBodyStyle: {
        margin: '0px',
        padding: '0px'
      },
      nzComponentParams: {
        projectId: this.projectId
      },
      nzOnOk: () => {
        console.log('nzOnOk');
      }
    });
    
    // modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe(result => {
      console.log('[afterClose] The result is:', result);
      if (!result) {
        return;
      }

      const exist = this.collaborators.find(c => result.id);
      if (!exist) {
        console.log('Add the collaborator')
        this.collaborators.push(result);
      } else {
        console.log('Collaborator already exists ... ', exist);
      }
    });
  }

}

@Component({
  selector: 'app-xorms-details-settings-collaborators-invitation',
  template: `
    <nz-spin [nzSpinning]="isLoading" [nzDelay]="500" *ngIf="!done">
      <div style='padding: 16px 24px'>
        <input type="email" nz-input [(ngModel)]="emailToInvite" placeholder="Enter the email" />
      </div>
      <div style='padding:0 24px'><strong>Choose a role</strong></div>
      <div style="border-top: 1px solid grey;">
        <div *ngFor="let role of roles" style="border-bottom: 1px solid grey; padding: 16px 24px;display: flex;flex-direction: row;align-items: start;">
          <input type="radio" name="roles" id="{{ role.value }}" value="{{ role.value }}" [(ngModel)]="selectedRole" />
          <label for="{{ role.value }}">
            <div><strong>{{ role.title }}</strong></div>
            <div>{{ role.description }}</div>
          </label>
        </div>
      </div>
      <div style="align: center">
        <button nz-button nzType="primary" nzBlock nzSize="large" [disabled]="!emailToinvite && !selectedRole" (click)="sendInvitation()">Invite</button>
      </div>
    </nz-spin>
    <div *ngIf="done">
      <nz-result [nzStatus]="errorOccurred ? 'error' : 'success'" [nzTitle]=" errorOccurred ? errorMessage : 'Collaborator invited!'">
        <div nz-result-extra>
          <button nz-button nzType="primary" (click)="destroyModal()">Close</button>
        </div>
      </nz-result>
    </div>
  `
})
export class InviteCollaboratorComponent {

  @Input() projectId: string;

  roles: {
    value: string,
    title: string,
    description: string
  }[] = [
    { value: "owner", title: "Owner", description: "Possesses all the rights on the project"},
    { value: "administrator", title: "Administrator", description: "Regulary take a look on the project"},
    { value: "supervisor", title: "Supervisor", description: "Supervises the collectors"},
    { value: "collector", title: "Collector", description: "Collects data from mobile application"}
  ];
  emailToInvite: string = '';
  selectedRole: string;

  isLoading: boolean = false;
  done: boolean = false;
  errorOccurred: boolean = false;
  errorMessage: string = '';

  collaborator: Collaborator;

  constructor(
    private modal: NzModalRef,
    private xormsService: XormsService,
  ) {}

  destroyModal(): void {
    this.modal.destroy(this.collaborator);
  }

  sendInvitation() {
    this.isLoading = true;

    this.xormsService.inviteCollaborator(this.projectId, this.emailToInvite, this.selectedRole).subscribe(
      (result: Collaborator) => {
        console.log("Result... FORM ... ", result);
        this.emailToInvite = '';

        this.collaborator = result;
        this.done = true;

        this.isLoading = false;
      },
      (error) => {
        this.done = true;
        this.errorOccurred = true;
        
        console.log(error);
        this.errorMessage = error;

        this.isLoading = false;
      }
    );
  }
}

      