import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDetailsSettingsCollaboratorsComponent } from './xorms-details-settings-collaborators.component';

describe('XormsDetailsSettingsCollaboratorsComponent', () => {
  let component: XormsDetailsSettingsCollaboratorsComponent;
  let fixture: ComponentFixture<XormsDetailsSettingsCollaboratorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XormsDetailsSettingsCollaboratorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDetailsSettingsCollaboratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
