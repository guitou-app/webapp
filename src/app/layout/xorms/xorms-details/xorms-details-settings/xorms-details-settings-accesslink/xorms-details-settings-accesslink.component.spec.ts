import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDetailsSettingsAccesslinkComponent } from './xorms-details-settings-accesslink.component';

describe('XormsDetailsSettingsAccesslinkComponent', () => {
  let component: XormsDetailsSettingsAccesslinkComponent;
  let fixture: ComponentFixture<XormsDetailsSettingsAccesslinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XormsDetailsSettingsAccesslinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDetailsSettingsAccesslinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
