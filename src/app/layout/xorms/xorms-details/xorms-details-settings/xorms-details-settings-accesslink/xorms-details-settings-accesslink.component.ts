import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { forkJoin } from 'rxjs';
import { first } from 'rxjs/operators';
import { Project } from 'src/app/_models/project';
import { Xorm } from 'src/app/_models/xorm';
import { XormsService } from 'src/app/_services/xorms.service';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-xorms-details-settings-accesslink',
  templateUrl: './xorms-details-settings-accesslink.component.html',
  styleUrls: ['./xorms-details-settings-accesslink.component.less']
})
export class XormsDetailsSettingsAccesslinkComponent implements OnInit {

  WEBSITE_URL: string = environment.websiteUrl;

  currentProject: Project;

  shouldTakeGeolocationPosition: { label: string, value: string, checked: boolean }[];
  allXormGeolocationPosition: boolean = false;
  indeterminateXormGeolocated: boolean = true;
  isGeolocationSaving: boolean = false;

  customURLId: { id: string; isSetUp: boolean; customURLId: string }[] = [];

  constructor(
    private notification: NzNotificationService,
    private modal: NzModalService,
    private xormsService: XormsService,
  ) { }

  ngOnInit(): void {
    this.xormsService.currentProject$.subscribe(project =>  {
      this.currentProject = project;

      this.loadingGeneralSetting();
    });
  }

  async loadingGeneralSetting() {
    let geolocationPosition = this.currentProject.settings.xorms.geolocationPosition;
    if (!geolocationPosition) {
      geolocationPosition = [];
    }
    this.shouldTakeGeolocationPosition = this.currentProject.xorms.map((x: string | Xorm) => {
      const g = geolocationPosition.find(g => g.xorm === (x as Xorm).id);

      return ({
        label: (x as Xorm).title,
        value: (x as Xorm).id,
        checked: g ? g.shouldTakeGeolocationPosition : false
      });
    });
    if (this.shouldTakeGeolocationPosition.every(item => item.checked)) {
      this.allXormGeolocationPosition = true;
      this.indeterminateXormGeolocated = false;
    }

    forkJoin(
      [
        ...this.currentProject.xorms.map(
          (x: string | Xorm) => this.xormsService.getCustomURLId(this.currentProject.id, (x as Xorm).id)
        )
      ]
    ).subscribe(
      (responses: string[]) => {
        for (let i=0; i < this.currentProject.xorms.length; i++) {
          const xormId = (this.currentProject.xorms[i] as Xorm).id;
          const currentResponse = responses[i];
          this.customURLId.push({
            id: xormId,
            isSetUp: currentResponse ? true : false,
            customURLId: currentResponse
          });
        }
      }
    )
  }

  updateSingleXormGeolocated() {
    if (this.shouldTakeGeolocationPosition.every(item => !item.checked)) {
      this.allXormGeolocationPosition = false;
      this.indeterminateXormGeolocated = false;
    } else if (this.shouldTakeGeolocationPosition.every(item => item.checked)) {
      this.allXormGeolocationPosition = true;
      this.indeterminateXormGeolocated = false;
    } else {
      this.indeterminateXormGeolocated = true;
    }
  }

  updateAllXormGeolocated() {
    this.indeterminateXormGeolocated = false;
    if (this.allXormGeolocationPosition) {
      this.shouldTakeGeolocationPosition = this.shouldTakeGeolocationPosition.map(item => ({
        ...item,
        checked: true
      }));
    } else {
      this.shouldTakeGeolocationPosition = this.shouldTakeGeolocationPosition.map(item => ({
        ...item,
        checked: false
      }));
    }
  }

  saveGeolocationPosition() {
    this.isGeolocationSaving = true;
    const settings = {
      geolocationPosition: this.shouldTakeGeolocationPosition.map(g => ({ 
        xorm: g.value,
        shouldTakeGeolocationPosition: g.checked
      }))
    }
    this.xormsService.updateProjectSettings(this.currentProject.id, settings).pipe(first()).subscribe(
      () => {
        this.isGeolocationSaving = false;

        this.notification
        .blank(
          'Settings',
          'Geolocation configuration saved'
        );

        window.location.reload();
      }
    );
  }

  onRemoveCustomURLId(i: number) {
    const removedCustomURLId = this.customURLId[i];
    if (!removedCustomURLId.isSetUp && removedCustomURLId.customURLId) {
      this.modal.create({
        nzTitle: 'Remove Custom URL ID',
        nzContent: 'You are about to remove the defined custom URL for this xorm. Is it really what you want to do?',
        nzClosable: false,
        nzOkText: "Yes",
        nzCancelText: "No, cancel",
        nzOnOk: () => this.saveCustomURLId(removedCustomURLId.id, -1)
      });
    }
  }

  saveCustomURLId(xormId: string, urlId: string | number) {
    this.xormsService.updateXormSettings(
      this.currentProject.id,
      xormId,
      { "customURLId": urlId }
    ).subscribe(
      response => {
        if (urlId === -1) {
          this.notification
            .blank(
              'Settings',
              'Custom URL Identified removed'
            );
        } else {
          this.notification
            .blank(
              'Settings',
              'Custom URL Identifier saved'
            );
        }
        window.location.reload();
      }
    )
  }

}
