import { Component, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';

import { environment } from '../../../../../environments/environment';

import { first } from 'rxjs/operators';

import { FormsService } from '../../../../_services/forms.service';
import { XormsService } from '../../../../_services/xorms.service';
import { Project } from 'src/app/_models/project';

@Component({
  selector: 'app-xorms-details-settings',
  templateUrl: './xorms-details-settings.component.html',
  styleUrls: ['./xorms-details-settings.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class XormsDetailsSettingsComponent implements OnInit {
  currentXorms: any = [];
  projectId: string;
  currentProject: Project;
  websiteUrl:string = environment.websiteUrl;
  enableUpdate: boolean = false;
  isSaving: boolean = false;
  customURLId: string;
  setCustomURLId: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private xormsService: XormsService,
    private notification: NzNotificationService
  ) {}

  ngOnInit() {
    this.projectId = this.activatedRoute.snapshot.parent.params['projectId'];
    this.getProject();
  }

  getProject() {
    this.xormsService.fetchOne(this.projectId).subscribe(
      (project:Project) => {
        console.log('Get Project ', project);

        this.currentProject = project;
      }
    );
  }

  general() {
    this.router.navigate(['general'], {relativeTo: this.activatedRoute});
  }

  collaborators() {
    this.router.navigate(['collaborators'], {relativeTo: this.activatedRoute});
  }

  languages($event?: MouseEvent, xormId?: string) {
    console.log('Event Xorm ', $event, xormId);
    if (xormId) {
      if (!this.currentProject.settings.defaultLanguage) {
        this.notification
        .blank(
          'Settings',
          'Kindly first set the default language'
        )
      } else {
        this.router.navigate(['languages'], {
          relativeTo: this.activatedRoute,
          queryParams: {
            xorm: xormId
          },
        });
      }
      $event.stopPropagation();

      return;
    }
    this.router.navigate(['languages'], {
      relativeTo: this.activatedRoute,
    });
  }

  onChange() {
    this.enableUpdate = true;
  }

  onSetCustomURLId() {
    this.setCustomURLId = true;
  }

  onSave() {
    this.isSaving = true;

    this.xormsService.update(this.projectId, this.currentProject).pipe(first()).subscribe(
      (result:any) => {
        
        console.log("Result... FORM ... ", result);
        // this.allForms = result.data;
        // console.log(this.allForms);
        
        // this.currentProject = result.data;
        // console.log(this.currentProject);

        this.isSaving = false;
        this.enableUpdate = false;
      }
    )
  }

}
