import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDetailsSettingsComponent } from './xorms-details-settings.component';

describe('XormsDetailsSettingsComponent', () => {
  let component: XormsDetailsSettingsComponent;
  let fixture: ComponentFixture<XormsDetailsSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsDetailsSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDetailsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
