import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDetailsSettingsLanguagesComponent } from './xorms-details-settings-languages.component';

describe('XormsDetailsSettingsLanguagesComponent', () => {
  let component: XormsDetailsSettingsLanguagesComponent;
  let fixture: ComponentFixture<XormsDetailsSettingsLanguagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XormsDetailsSettingsLanguagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDetailsSettingsLanguagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
