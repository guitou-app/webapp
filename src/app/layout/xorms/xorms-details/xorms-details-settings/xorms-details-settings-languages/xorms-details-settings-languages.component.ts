import { Component, Input, OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Project, ProjectTranslations } from 'src/app/_models/project';
import { Xorm, XormQuestionDatatable, XormQuestionMultipleChoice, XormTranslations } from 'src/app/_models/xorm';
import { XormsService } from 'src/app/_services/xorms.service';

@Component({
  selector: 'app-xorms-details-settings-languages',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './xorms-details-settings-languages.component.html',
  styleUrls: ['./xorms-details-settings-languages.component.less'],
})
export class XormsDetailsSettingsLanguagesComponent implements OnInit {

  currentProject: Project;
  currentXorm: Xorm | undefined;
  currentXormId: string | undefined;

  isDefaultLanguageModalVisible: boolean = false;
  isAddLanguageModalVisible: boolean = false;
  isProjectInfosLanguageVisible: boolean = false;
  isAddNewLanguageModalVisible: boolean = false;

  defaultLanguage: string;
  nameOfNewLanguage: string;
  isDLWorking: boolean = false;

  constructor(
    private router: Router,
    private notification: NzNotificationService,
    private activatedRoute: ActivatedRoute,
    private xormsService: XormsService,
    private modal: NzModalService,
    private viewContainerRef: ViewContainerRef
  ) {
    this.activatedRoute.queryParamMap
      .subscribe((params) => {
        this.currentXormId = params.get("xorm");
        if (this.currentXormId) {
          this.getXormById();
        } else {
          this.currentXorm = undefined;
          this.currentXormId = undefined;
        }
      }
    );
  }

  ngOnInit(): void {
    this.xormsService.currentProject$.subscribe(project =>  {
      this.currentProject = project;
      this.defaultLanguage = this.currentProject.settings.defaultLanguage;

      if (this.currentXormId) {
        this.getXormById();
      }
    });
  }

  getXormById() {
    if (!(this.currentProject && this.currentXormId)) {
      return;
    }

    this.xormsService.getXormById(this.currentProject.id, this.currentXormId).pipe(first()).subscribe(
      (xorm) => {
        this.currentXorm = xorm;
      }
    )
  }

  openDefaultLanguageModal() {
    this.isDefaultLanguageModalVisible = true;
  }

  closeDefautLanguageModal() {
    this.isDefaultLanguageModalVisible = false;
  }

  setDefaultLanguage() {
    this.isDLWorking = true;
    const settings = {
      defaultLanguage: this.defaultLanguage
    }
    this.xormsService.updateProjectSettings(this.currentProject.id, settings).pipe(first()).subscribe(
      (xorm) => {
        this.isDLWorking = false;

        this.notification
        .blank(
          'Settings',
          'Default language saved'
        )

        // this.router.navigate([this.router.url])
        window.location.reload();
      }
    )
  }

  openAddNewLanguageModal() {
    this.isAddLanguageModalVisible = true;
  }

  openProjectInformationModal(language: string) {
    const isProjectOrXorm = !(this.currentXorm || this.currentXormId);
    const data = this.currentXormId
      ? this.currentXorm.translations[language]
      : this.currentProject.translations[language]
    ;
    const modal = this.modal.create({
      nzContent: DisplayTranslationLanguageComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        // projectId: this.currentProject.id,
        // xormId: this.currentXormId,
        language,
        isXormOrProject: isProjectOrXorm ? 1 : 2,
        data
      },
      nzFooter: null
    });

    modal.afterOpen.subscribe(() => {
      this.isAddNewLanguageModalVisible = false;
    });
    // Return a result when closed
    modal.afterClose.subscribe(result => {
      if (!result) {
        return;
      }
      if (result.hasOwnProperty('edit')) {
        this.openTranslateInLanguageModal(language, true, data);
      } else if (result.hasOwnProperty('nothing')) {

      }
      // window.location.reload();
    });
  }

  closeProjectInfosLanguageModal() {
    this.isProjectInfosLanguageVisible = false;
  }

  openAskNewLanguageName() {
    this.isAddNewLanguageModalVisible = true;
  }

  closeAskNewLanguageName() {
    this.isAddNewLanguageModalVisible = false;
  }

  submitNewLanguageName() {
    this.isAddNewLanguageModalVisible = false;

    const isProjectOrXorm = !(this.currentXorm || this.currentXormId);
    const modal = this.modal.create({
      nzContent: AddNewLanguageComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        projectId: this.currentProject.id,
        xormId: this.currentXormId,
        newLanguageName: this.nameOfNewLanguage,
        isTranslatingXormOrProject: isProjectOrXorm ? 1 : 2,
        data: this.currentXormId
          ? this.currentXorm.translations[this.currentProject.settings.defaultLanguage]
          : this.currentProject.translations[this.currentProject.settings.defaultLanguage]
      },
      nzFooter: null
    });

    modal.afterOpen.subscribe(() => {
      this.isAddNewLanguageModalVisible = false;
    });
    // Return a result when closed
    modal.afterClose.subscribe(result => {
      window.location.reload();
    });
  }

  openTranslateInLanguageModal(
    language: string,
    edit: boolean = false,
    translation: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations] = undefined
  ) {
    const isProjectOrXorm = !(this.currentXorm || this.currentXormId);
    const modal = this.modal.create({
      nzContent: AddNewLanguageComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        edit,
        translation,
        projectId: this.currentProject.id,
        xormId: this.currentXormId,
        newLanguageName: language,
        isTranslatingXormOrProject: isProjectOrXorm ? 1 : 2,
        data: this.currentXormId
          ? this.currentXorm.translations[this.currentProject.settings.defaultLanguage]
          : this.currentProject.translations[this.currentProject.settings.defaultLanguage]
      },
      nzWidth: '80vw',
      nzFooter: null
    });

    modal.afterOpen.subscribe((result) => {
      if (result !== undefined && result["success"]) {
        this.isAddNewLanguageModalVisible = false;
      }
    });
    // Return a result when closed
    modal.afterClose.subscribe(result => {
      console.log('[afterClose] The result is:', result);
      // window.location.reload();
    });
  }

}

@Component({
  selector: 'app-add-new-language-modal',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './add-new-language-modal.html',
  styleUrls: ['./add-new-language-modal.less']
})
export class AddNewLanguageComponent implements OnInit {
  @Input()
  edit: boolean;
  @Input()
  translation: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations];

  @Input()
  projectId: string;
  @Input()
  xormId: string;
  @Input()
  newLanguageName: string;
  @Input()
  isTranslatingXormOrProject: number;
  @Input()
  data: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations];
  
  isTranslatingXorm: boolean;
  isTranslatingProject: boolean;
  validateForm!: FormGroup;
  isSaving: boolean = false;

  currentXormSection: number = 0;

  constructor(
    private modal: NzModalRef,
    private fb: FormBuilder,
    private xormService: XormsService
  ) {}

  ngOnInit(): void {
    if (this.isTranslatingXormOrProject == 1) {
      this.validateForm = this.fb.group({
        title: [
          this.edit ? this.translation['title'] : null,
          [Validators.required]
        ],
        description: [
          this.edit ? this.translation['description'] : '',
          []
        ],
      });
    } else {
      this.validateForm = this.fb.group({
        title: [
          this.edit ? this.translation['title'] : null,
          [Validators.required]
        ],
        xorm: this.fb.group({})
      });
      const xorm = (this.data as XormTranslations[keyof XormTranslations]).xorm;
      const xormTranlation = this.edit ? (this.translation as XormTranslations[keyof XormTranslations]).xorm : undefined;
      const xormFormGroup = this.fb.group({});
      for (let sectionKey in xorm) {
        const sectionFormGroup = this.fb.group({
          _params: this.fb.group({
            key: sectionKey,
            title: [
              this.edit ? xormTranlation[sectionKey]._params.title : null,
              [], // [Validators.required]
            ],
            description: [
              this.edit ? xormTranlation[sectionKey]._params['description'] : '',
              []
            ],
          }),
          questions: this.fb.group({})
        });

        const questions = xorm[sectionKey].questions;
        const questionFormGroup = this.fb.group({});
        for(let questionKey in questions) {
          const question = xormTranlation[sectionKey].questions[questionKey];
          const { title, description, type } =  question;
          
          if (
            type === 'multiple-choice' ||
            type === 'single-choice' ||
            type === 'single-choice-select' ||
            type === 'yes-no' ||
            type === 'yes-no-dont'
          ) {
            const { options } = question as XormQuestionMultipleChoice;
            const optionsTranslated = options.map((opt) => (this.fb.group({ 
              "key": [opt['key'], []],
              "value": [
                this.edit ? opt['value'] : '',
                []
              ]
            })));
            questionFormGroup.addControl(
              questionKey,
              this.fb.group({
                title: [
                  this.edit ? title : null,
                  [] // [Validators.required]
                ],
                description: [
                  this.edit ? description : '',
                  []
                ],
                options: this.fb.array(optionsTranslated)
              })
            );
          } else if (type === 'datatable') {
            const { cols, rows } = question as XormQuestionDatatable;
            const colsTranslated = cols.map(c => (this.fb.group({
              type: [c['type'], []],
              text: [
                this.edit ? c['text'] : ''
              ]
            })));
            const rowsTranslated = rows.map(c => (this.fb.group({
              type: [c['type'], []],
              text: [
                this.edit ? c['text'] : ''
              ]
            })));
            questionFormGroup.addControl(
              questionKey,
              this.fb.group({
                title: [
                  this.edit ? title : null,
                  [] // [Validators.required]
                ],
                description: [
                  this.edit ? description : '',
                  []
                ],
                cols: this.fb.array(colsTranslated),
                rows: this.fb.array(rowsTranslated)
              })
            );
          } else {
            questionFormGroup.addControl(
              questionKey,
              this.fb.group({
                title: [
                  this.edit ? title : null,
                  [] // [Validators.required]
                ],
                description: [
                  this.edit ? description : '',
                  []
                ]
              })
            );
          }
        }
        sectionFormGroup.setControl('questions', questionFormGroup);
        xormFormGroup.addControl(sectionKey, sectionFormGroup);
      }
      this.validateForm.setControl("xorm", xormFormGroup);
    }

    this.isTranslatingProject = this.isTranslatingXormOrProject == 1;
    this.isTranslatingXorm = this.isTranslatingXormOrProject == 2;
  }

  getFromGroup(section: string): FormGroup {
    return <FormGroup>this.validateForm.get(section);
  }

  getQuestionOptions(section: string, question: string) {
    return this.validateForm.get('xorm').get(section).get('questions').get(question).get('options') as FormArray
  }

  getQuestionCols(section: string, question: string) {
    return this.validateForm.get('xorm').get(section).get('questions').get(question).get('cols') as FormArray
  }

  getQuestionRows(section: string, question: string) {
    return this.validateForm.get('xorm').get(section).get('questions').get(question).get('rows') as FormArray
  }

  onStepClicked(step: number): void {
    this.currentXormSection = step;
  }

  previousStep() {
    if (this.currentXormSection > 0) {
      this.currentXormSection -= 1;
    }
  }

  nextStep() {
    if (this.currentXormSection < Object.keys((this.data as XormTranslations[keyof XormTranslations]).xorm).length - 1) {
      this.currentXormSection += 1;
    }
  }

  submitForm(): void {
    console.log('submit ', this.validateForm.valid);
    console.log(this.validateForm.value);
    if (this.validateForm.valid) {
      const body = {
        language: this.newLanguageName,
        data: this.validateForm.value
      }
      this.isSaving = true;

      if (this.isTranslatingProject) {
        this.xormService.setProjectTranslation(this.projectId, body).subscribe(
          (result) => {
            this.isSaving = false;
            this.modal.destroy({ success: true });
          },
          (error) => {
            console.error(error);
            this.modal.destroy({ success: false });
          }
        );
      } else if (this.isTranslatingXorm) {
        console.log('is xorm translating : ', body);
        this.xormService.setXormTranslation(this.projectId, this.xormId, body).subscribe(
          (result) => {
            this.isSaving = false;
            this.modal.destroy({ success: true });
          },
          (error) => {
            console.error(error);
            this.modal.destroy({ success: false });
          }
        );
      }
    } else {
      if (this.isTranslatingProject) {
        Object.values(this.validateForm.controls).forEach(control => {
          if (control.invalid) {
            control.markAsDirty();
            control.updateValueAndValidity({ onlySelf: true });
          }
        });
      } else if (this.isTranslatingXorm) {
        // Object.values(this.validateForm.controls).forEach(control => {
        //   if (control instanceof FormGroup) {
        //     Object.values(control.controls).forEach(subControl => {
        //       if (subControl instanceof FormControl && subControl.invalid) {
        //         subControl.markAsDirty();
        //         subControl.updateValueAndValidity({ onlySelf: true });
        //       }
        //     })
        //   } else {
        //     if (control.invalid) {
        //       control.markAsDirty();
        //       control.updateValueAndValidity({ onlySelf: true });
        //     }
        //   }
        // });
      }
    }
  }

}

@Component({
  selector: 'app-display-translation-language-modal',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './display-translation-language-modal.html',
  styleUrls: ['./display-translation-language-modal.less']
})
export class DisplayTranslationLanguageComponent implements OnInit {
  @Input()
  language: string;
  @Input()
  isXormOrProject: number;
  @Input()
  data: ProjectTranslations[keyof ProjectTranslations] | XormTranslations[keyof XormTranslations];

  constructor(private modal: NzModalRef) {}

  ngOnInit(): void {}

  close() {
    this.modal.close({ nothing: true });
  }

  edit() {
    this.modal.close({ edit: true });
  }
}