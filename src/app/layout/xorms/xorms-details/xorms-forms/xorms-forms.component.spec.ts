import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsFormsComponent } from './xorms-forms.component';

describe('XormsFormsComponent', () => {
  let component: XormsFormsComponent;
  let fixture: ComponentFixture<XormsFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
