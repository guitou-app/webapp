import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { first } from 'rxjs/operators';

import { FormsService } from '../../../../_services/forms.service';
import { SocketIoService } from '../../../../_services/socket-io.service';
import { Xorm } from 'src/app/_models/xorm';

@Component({
  selector: 'app-xorms-forms',
  templateUrl: './xorms-forms.component.html',
  styleUrls: ['./xorms-forms.component.less']
})
export class XormsFormsComponent implements OnInit {

  allXorms: Xorm[] = [];
  projectId: string;

  error: string;
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formsService: FormsService,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    this.projectId = this.activatedRoute.parent.snapshot.params['projectId'];
    this.loadXorms();
  }

  loadXorms() {
    this.isLoading = true;
    this.formsService.fetchAll(this.projectId).pipe(first()).subscribe(
      (result:Xorm[]) => {
        this.isLoading = false;
        
        this.allXorms = result;
        console.log(this.allXorms);
      }
    )
  }

  removeXorm(id:string) {
    this.isLoading = true;
    this.formsService.remove(this.projectId, id).pipe(first()).subscribe(
      (result) => {
        this.isLoading = false;
        this.allXorms = this.allXorms.filter((x:Xorm) => x.id !== id);
      },
      (error) => {
        this.isLoading = true;
      }
    )
  }

  onAddXorm() {
    this.isLoading = true;
    this.formsService.create(this.projectId).pipe(first()).subscribe(
      (data:Xorm) => {
        this.isLoading = false;
        this.router.navigate([data.id, 'edit'], {relativeTo: this.activatedRoute});
      },
      (err) => {
        this.isLoading = false;
        this.error = "Sorry, an error occured"
      }
    )
  }

  onXorm(xorm) {
    console.log("onXorm... ", xorm._id);
    this.router.navigate([xorm._id, 'edit'], {relativeTo: this.activatedRoute});
  }
}
