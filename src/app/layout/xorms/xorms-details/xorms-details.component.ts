import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

import { first, multicast } from 'rxjs/operators';
import * as FileSaver from 'file-saver'

import { XormsService } from '../../../_services/xorms.service';
import { Project } from 'src/app/_models/project';


@Component({
  selector: 'app-xorms-details',
  templateUrl: './xorms-details.component.html',
  styleUrls: ['./xorms-details.component.less']
})
export class XormsDetailsComponent implements OnInit {

  projectId:string;
  currentProject:Project;

  isGeneratingAPK: boolean = false;
  messagesFromGeneratingAPK: string[] = [];

  constructor(
    private _location: Location,
    private activatedRoute: ActivatedRoute,
    private xormsService: XormsService,
  ) { }

  ngOnInit() {
    this.currentProject = this.activatedRoute.snapshot.data.project as Project;
    console.log('current project : ', this.currentProject);
    this.projectId = this.currentProject.id;

    this.xormsService.buildApk.subscribe(event => {
      console.log('generator.build-apk: ', event);
      this.isGeneratingAPK = true;
    });

    this.xormsService.buildApkProcessing.subscribe(event => {
      console.log('generator.build-apk.processing: ', event)
      this.messagesFromGeneratingAPK.push(event.message);
    });

    this.xormsService.buildApkEnd.subscribe(event => {
      console.log('generator.build-apk.end: ', event);
      this.isGeneratingAPK = false;
    });
  }

  getProject() {
    this.xormsService.fetchOne(this.projectId).pipe(first()).subscribe(
      (result:Project) => {
        console.log(result);

        this.currentProject = result;
      }
    );
  }

  goBack() {
    this._location.back();
  }

  generateApp() {
    console.log('current project : ', this.currentProject);
    console.log('Generate the APP ', this.projectId);
    this.xormsService.generate(this.projectId).pipe(first()).subscribe(
      (result) => {
        console.log('Generate APP : The result', result);
      }
    )
  }

  downloadApk(apkId: string = "") {
    console.log("downloadApk()");

    this.xormsService.downloadApk(this.projectId, apkId).pipe(first()).subscribe(
      (file) => {
        const filename = `${this.currentProject.title}_${Date.now().toString()}.apk`;
        console.log('filename : ', filename);

        let blob = new Blob([file], {
          type: 'application/vnd.android.package-archive'
        })

        FileSaver.saveAs(blob, filename);
      }, 
      (error: any) => console.log('Error downloading the file'), //when you use stricter type checking
      // () => console.info('File downloaded successfully');
    )
  }
}
