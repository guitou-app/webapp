import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { XormsComponent } from './xorms.component';
import { AllXormsComponent } from './all-xorms/all-xorms.component';
import { NewXormComponent } from './new-xorm/new-xorm.component';
import { XormsDetailsComponent } from './xorms-details/xorms-details.component';
import { XormsFormsComponent } from './xorms-details/xorms-forms/xorms-forms.component';
// import { JoinComponent } from './join/join.component';
// import { XormsDataNewComponent } from './xorms-data/xorms-data-new/xorms-data-new.component';
import { StatsComponent } from './stats/stats.component';
import { XormsDetailsSettingsComponent } from './xorms-details/xorms-details-settings/xorms-details-settings.component';

import { XormsDataModule } from './xorms-data/xorms-data.module';

import { AuthGuard } from '../../_guards/auth.guard';
import { ProjectResolver } from 'src/app/_resolvers/project.resolver';
import { XormsDetailsSettingsCollaboratorsComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-collaborators/xorms-details-settings-collaborators.component';
import { XormsDetailsSettingsAccesslinkComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-accesslink/xorms-details-settings-accesslink.component';
import { XormsDetailsSettingsLanguagesComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-languages/xorms-details-settings-languages.component';

const routes: Routes = [{
    path: '',
    component: XormsComponent,
    children: [
      { path: '', component: AllXormsComponent },
      {
        path: ':projectId',
        component: XormsDetailsComponent,
        resolve: {
          project: ProjectResolver
        },
        children: [
          { path: '', redirectTo: 'xorms', pathMatch: 'full'},
          { path: 'xorms', component: XormsFormsComponent },
          { path: 'xorms/:xormId/edit', component: NewXormComponent },
          { path: 'data', loadChildren: () => import('./xorms-data/xorms-data.module').then(x => x.XormsDataModule) },
          { path: 'dashboard', component: StatsComponent },
          {
            path: 'settings',
            component: XormsDetailsSettingsComponent,
            children: [
              { path: 'general', component: XormsDetailsSettingsAccesslinkComponent },
              { path: 'collaborators', component: XormsDetailsSettingsCollaboratorsComponent },
              { path: 'languages', component: XormsDetailsSettingsLanguagesComponent },
            ],
          },
          { path: 'stats', component: StatsComponent },
        ]
      },
    ],
    canActivate: [ AuthGuard ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XormsRoutingModule { }
