import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';

import { XormsRoutingModule } from './xorms-routing.module';
import { XormsComponent } from './xorms.component';
import { AllXormsComponent, NewModalXormComponent } from './all-xorms/all-xorms.component';
import { StatsComponent, NewChartDialog } from './stats/stats.component';

import { PieChartComponent } from './stats/pie-chart/pie-chart.component';
import { BarChartComponent } from './stats/bar-chart/bar-chart.component';
import { HistogramComponent } from './stats/histogram/histogram.component';
import { XormsDetailsComponent } from './xorms-details/xorms-details.component';
import { XormsDetailsSettingsComponent } from './xorms-details/xorms-details-settings/xorms-details-settings.component';

import { XormsDataModule } from './xorms-data/xorms-data.module';
import { NewXormComponent } from './new-xorm/new-xorm.component';
import { XormSectionComponent } from './new-xorm/xorm-section/xorm-section.component';
import { XormTitleDescComponent } from './new-xorm/xorm-title-desc/xorm-title-desc.component';
import { BoxplotComponent } from './stats/boxplot/boxplot.component';
import { XormsFormsComponent } from './xorms-details/xorms-forms/xorms-forms.component';
import { InviteCollaboratorComponent, XormsDetailsSettingsCollaboratorsComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-collaborators/xorms-details-settings-collaborators.component';
import { XormQuestionSingleChoiceSelectComponent } from './new-xorm/xorm-question-single-choice-select/xorm-question-single-choice-select.component';
import { XormsDetailsSettingsAccesslinkComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-accesslink/xorms-details-settings-accesslink.component';
import { XormsDetailsSettingsLanguagesComponent, AddNewLanguageComponent, DisplayTranslationLanguageComponent } from './xorms-details/xorms-details-settings/xorms-details-settings-languages/xorms-details-settings-languages.component';

// import { XormsDataNewComponent } from './xorms-data/xorms-data-new/xorms-data-new.component';

@NgModule({
  declarations: [
    XormsComponent,
    AllXormsComponent, NewModalXormComponent,
    StatsComponent, NewChartDialog,
    XormsDetailsComponent,

    PieChartComponent, BarChartComponent, HistogramComponent, XormsDetailsSettingsComponent, NewXormComponent, XormSectionComponent, XormTitleDescComponent, BoxplotComponent, XormsFormsComponent, 
    XormsDetailsSettingsCollaboratorsComponent, InviteCollaboratorComponent, XormQuestionSingleChoiceSelectComponent, XormsDetailsSettingsAccesslinkComponent, XormsDetailsSettingsLanguagesComponent,
    AddNewLanguageComponent, DisplayTranslationLanguageComponent
     //, XormsDataComponent, XormsDataNewComponent
  ],
  imports: [
    CommonModule,
    XormsRoutingModule,
    FormsModule, ReactiveFormsModule,

    SharedModule,
    XormsDataModule
  ],
  entryComponents: [
    NewChartDialog,
    NewModalXormComponent, 
    InviteCollaboratorComponent
  ]
})
export class XormsModule { }
