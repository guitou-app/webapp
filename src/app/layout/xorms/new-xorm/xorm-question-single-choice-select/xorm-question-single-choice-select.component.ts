import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { XormQuestionOption, XormQuestionSingleChoiceSelect } from 'src/app/_models/xorm';

@Component({
  selector: 'div[xorm-question-single-choice-select]',
  templateUrl: './xorm-question-single-choice-select.component.html',
  styleUrls: ['./xorm-question-single-choice-select.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class XormQuestionSingleChoiceSelectComponent implements OnInit {

  @Input() question: XormQuestionSingleChoiceSelect;

  constructor() { }

  ngOnInit(): void {
    console.log('[SCS] ', this.question);
  }

  addOption() {
    this.question.addOption();
  }

  onRemoveOption(o: number) {
    this.question.onRemoveOption(o);
  }
}
