import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XormQuestionSingleChoiceSelectComponent } from './xorm-question-single-choice-select.component';

describe('XormQuestionSingleChoiceSelectComponent', () => {
  let component: XormQuestionSingleChoiceSelectComponent;
  let fixture: ComponentFixture<XormQuestionSingleChoiceSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XormQuestionSingleChoiceSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XormQuestionSingleChoiceSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
