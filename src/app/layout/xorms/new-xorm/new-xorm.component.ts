import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { first } from 'rxjs/operators';

import { FormsService } from '../../../_services/forms.service';
import { SocketIoService } from '../../../_services/socket-io.service';
import { Xorm, XormQuestion, XormQuestionDatatable, XormQuestionMultipleChoice, XormQuestionSingleChoice, XormQuestionSingleChoiceSelect, XormQuestionString, XormQuestionText, XormQuestionTitleDesc, XormQuestionYesNo, XormSection, XormSectionParams } from 'src/app/_models/xorm';
import { stringify } from 'querystring';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-new-xorm',
  templateUrl: './new-xorm.component.html',
  styleUrls: ['./new-xorm.component.less']
})
export class NewXormComponent implements OnInit {

  title:string = "Untitled form";
  level:string;
  xorm:Xorm;
  xormBody: XormSection[] = []; // Record<string, any>;
  projectId:string;
  xormId:string;
  currentXormSection:number = 0;
  currentSectionQuestion: number = -1;

  allOptions = [];

  isEditingOrVisualizing = false;

  constructor(
    private _location: Location,
    private activatedRoute: ActivatedRoute,
    private formsService: FormsService,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    this.projectId = this.activatedRoute.snapshot.parent.params['projectId'];
    this.xormId = this.activatedRoute.snapshot.params['xormId'];
    console.log(this.projectId, ' ---- ', this.xormId, this.activatedRoute.snapshot.parent);

    if (this.projectId) {
      this.loadXorm();
    }
  }

  saveXorm() {
    let params = {
      values: this.xormTransformation()
    };
    console.log(params);
    this.formsService.update(this.projectId, this.xormId, params).pipe(first()).subscribe(
      (data) => { 
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  loadXorm() {
    this.formsService.fetchOne(this.projectId, this.xormId).pipe(first()).subscribe(
      (data:Xorm) => {
        console.log('loadXorm ', data);
        
        console.log(data);
        this.xorm = data;
        this.title = data.title;
        this.level = data.level;
        if (!data.xorm) {
          this.xormBody = [new XormSection('section_0')];
          return;
        }
        console.log(this.xormBody, this.title);
        
        this.xormBody = Object.keys(this.xorm.xorm).map(key => XormSection.fromJSON(key, this.xorm.xorm[key]));

        console.log("Usable Loaded Data : ", this.xormBody);
      }
    )
  }

  goBack() {
    this._location.back();
  }

  getAllOptions() {
    return [];
  }

  addQuestion(params) {
    const { type, position, currentIndex } = params;
    console.log('addQuestion ', params);
    let index: number = currentIndex;
    if (position === 'after') {
      index = currentIndex + 1;
    }

    this.xormBody[this.currentXormSection].questions.splice(
      index,
      0,
      new XormQuestionString({
        id: `section_${this.currentXormSection}__${this.xormBody[this.currentXormSection].questions.length}`
      })
    );
  }

  addSection(params) {
    const { position, currentIndex } = params;

    let index: number = currentIndex;
    if (position === 'after') {
      index = currentIndex + 1;
    }

    this.xormBody.splice(index, 0, new XormSection(`section_${this.xormBody.length}`));
  }

  addTitleDescription(params) {
    const { position, currentIndex } = params;

    let index: number = currentIndex;
    if (position === 'after') {
      index = currentIndex + 1;
    }

    this.xormBody[this.currentXormSection].questions.splice(
      index,
      0,
      new XormQuestionTitleDesc({
        id: `section_${this.currentXormSection}__${this.xormBody[this.currentXormSection].questions.length}`
      })
    );
  }

  onAddItem($event) {
    let itemType = $event;

    switch (itemType) {
      case 0: // Question
        this.xormBody[this.currentXormSection].questions.push(new XormQuestionString({
          id: `section_${this.currentXormSection}__${this.xormBody[this.currentXormSection].questions.length}`
        }))
        console.log(this.xormBody[this.currentXormSection].questions);
        break;
      case 1: // Title && Description
        this.xormBody[this.currentXormSection].questions.push(new XormQuestionTitleDesc({
          id: `section_${this.currentXormSection}__${this.xormBody[this.currentXormSection].questions.length}`
        }))
        console.log(this.xormBody[this.currentXormSection].questions);

        break;
      case 2: // Section
        this.xormBody.push(new XormSection(`section_${this.xormBody.length}`));

        break;
      default:
    }
    this.currentSectionQuestion = this.xormBody[this.currentXormSection].questions.length - 1;
  }

  onChangeXormSectionItem($event) {}

  onXormSectionSelected($event) {
    let params = $event;

    this.currentXormSection = params.sectionIndex - 1;
  }

  onDuplicatingSection($event) {
    let params = $event;
    let s = JSON.parse(JSON.stringify(this.xormBody[this.currentXormSection]));
    let key = `section_${this.xormBody.length}`;
    s._params.key = key;
    s.questions = s.questions.map((q, ix) => {
      q.key = `${key}__${ix}`;
      return q;
    });
    this.xormBody.splice(this.currentXormSection + 1, 0, s);
  }

  onDeletingSection($event) {
    if (this.xormBody.length <= 1) {
      return;
    }

    let params = $event;
    this.xormBody.splice(this.currentXormSection, 1);
  }

  onDuplicatingQuestion($event) {
    let params = $event;
    let q = JSON.parse(JSON.stringify(this.xormBody[params.sectionIndex - 1].questions[params.question]));
    q.key = `section_${this.currentXormSection}__${this.xormBody[this.currentXormSection].questions.length}`;
    this.xormBody[params.sectionIndex - 1].questions.splice(params.question + 1, 0, q);
  }

  onDeletingQuestion($event) {
    let params = $event;
    this.xormBody[params.sectionIndex - 1].questions.splice(params.question, 1);
  }

  xormTransformation() {
    let finalXorm = {
      title: this.title,
      xorm: {} // RemoteXormSection: key, XormSection
    };

    for (let i=0; i < this.xormBody.length; i++) {
      let section: XormSection = this.xormBody[i];
      let rsection: {
        _params: XormSectionParams,
        questions: Record<string, XormQuestion>
      } = {
        ...section,
        questions: {}
      };

      for (let j=0; j < this.xormBody[i].questions.length; j++) {
        const question = this.xormBody[i].questions[j];
        console.log("Question... ", question);

        rsection.questions[question.id] = question;
      }

      // section._params.key = section._params.key.replace(/ /g, "_");
      finalXorm.xorm[section._params.key] = rsection;
    }

    return finalXorm;
  }

  onSwitchEdition() {
    if (!this.isEditingOrVisualizing) {
      this.saveXorm();
    }
  }

  toggleVisualization() {
    this.isEditingOrVisualizing = !this.isEditingOrVisualizing; 
    this.onSwitchEdition();
  }

  onSave() {
    this.saveXorm();
  }

}
