import { Component, OnInit, ElementRef, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
import { XormQuestion, XormQuestionDatatable, XormQuestionDate, XormQuestionMultipleChoice, XormQuestionNumber, XormQuestionOption, XormQuestionSingleChoice, XormQuestionSingleChoiceSelect, XormQuestionString, XormQuestionText, XormQuestionTime, XormQuestionWithOptions, XormQuestionYesNo, XormQuestionYesNoDont } from 'src/app/_models/xorm';

@Component({
  selector: 'app-xorm-section',
  templateUrl: './xorm-section.component.html',
  styleUrls: ['./xorm-section.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class XormSectionComponent implements OnInit {

  @Input()
  isEditingOrVisualizing: boolean;

  @Input()
  xormSection: any;

  @Input()
  numberOfSection: number;

  @Input()
  isEnabled: boolean;

  @Input()
  sectionIndex: number;

  @Output()
  changeXormSectionItem = new EventEmitter<any>();

  @Output()
  selected = new EventEmitter<any>();

  @Input()
  currentQuestion = -1;

  @Input()
  allOptions: any = [];

  @Output()
  duplicateQuestion = new EventEmitter<any>();

  @Output()
  deleteQuestion = new EventEmitter<any>();

  @Output()
  duplicateSection = new EventEmitter<any>();

  @Output()
  deleteSection = new EventEmitter<any>();

  @Output()
  addQuestion = new EventEmitter<any>();

  @Output()
  addSection = new EventEmitter<any>();

  @Output()
  addTitleDescription = new EventEmitter<any>();

  hoverQuestion: number = -1;

  questionType = {
    string: 'Short answer',
    text: 'Long answer',
    qmor: 'Question with Multiple Open Responses', // Question a Reponses Ouvertes Multiples
    number: 'Number',
    'yes-no': 'Yes/No',
    'yes-no-dont': 'Yes/No/I don\'t know',
    'single-choice': 'Single choice',
    'multiple-choice': 'Multiple choice',
    'single-choice-select': 'Select choice',
    date: 'Date',
    time: 'Time',
    datatable: 'Data table', // Table de donnees
    scale: 'Linear scale',
    rate: 'Rating'
  };

  date = null;
  time: Date | null = null;
  defaultOpenValue = new Date(0, 0, 0, 0, 0, 0);
  stringType = ['Number', 'String'];
  datatableCustomTypes = [
    'Text',
    'Number',
    'Yes/No',
    'Single Choice',
  ];

  constructor() { }

  ngOnInit() {
    console.log('xorm-section ', this.xormSection);

    $(document).ready(function() {
        $.each($('textarea'), function() {
          let offset = this.offsetHeight - this.clientHeight;

          let resizeTextarea = function(el) {
            $(el).height(0).height(el.scrollHeight);
          };
          $(this).on('change keyup keydown paste cut', function() { resizeTextarea(this); });
        });
    });
  }

  onSelectXormSection(e: any) {
    // const xormsPosition = $('.xorms')[0].getBoundingClientRect();
    // const path = e.path;
    // let p = 0;
    // while (p < path.length && !$(path[p]).hasClass('section')) {  p++; }
    // const position = $(path[p])[0].getBoundingClientRect();
    // $('.actions').css({ top: position.top - xormsPosition.top });

    this.selected.emit({sectionIndex: this.sectionIndex, itemType: 'section', eltRef: e});
  }

  onDuplicateSection() {
    this.duplicateSection.emit();
  }

  onDeleteSection() {
    this.deleteSection.emit();
  }

  onSettings() {

  }

  drop(event: CdkDragDrop<any[]>) {
    moveItemInArray(this.xormSection.questions, event.previousIndex, event.currentIndex);
  }

  onAddQuestion(type, position, currentIndex) {
    this.addQuestion.emit({type, position, currentIndex});
  }

  onAddSection(position, currentIndex) {
    this.addSection.emit({position, currentIndex})
  }

  onAddTitleDescription(position, currentIndex) {
    this.addTitleDescription.emit({position, currentIndex});
  }

  onQuestion(e:any, q: number) {
    this.currentQuestion = q;
  }

  onMouseoverQuestion($event, index) {
    this.hoverQuestion = index;
  }

  onMouseoutQuestion($event) {
    this.hoverQuestion = -1;
  }

  onSwitchToArray($event, sindex) {
    if (this.xormSection._params.tarray) {
      this.xormSection._params.tarray_question = '0__fix__0';

      const fullOptions = this.getAllPreOptions();
      if (fullOptions.length == 0) {
        this.xormSection._params.tarray_max_times_fixed = 0;
      }
    }
  }

  onTypeChanged(newType, ix) {
    let newQuestion: XormQuestion;

    let question = this.xormSection.questions[ix] as XormQuestion;
    console.log('[onTypeChanged] ', question);
    console.log(newType, ix);

    switch (newType) { // } (this.xormSection.questions[ix].type) {
      case 'string': // question instanceof XormQuestionString:
        this.xormSection.questions[ix] = new XormQuestionString({
          ...question
        });
        break;
      case 'text': // question instanceof XormQuestionText: // 'text':
        this.xormSection.questions[ix] = new XormQuestionText({
          ...question
        });
        break;
      case 'number': // question instanceof XormQuestionNumber: //'number':
        this.xormSection.questions[ix] = new XormQuestionNumber({
          ...question
        });
        break;
      case 'yes-no':
        const ynOptions = [
          {key: 'Yes', value: 'Yes', jumpTo: ''},
          {key: 'No', value: 'No', jumpTo: ''}
        ];
        this.xormSection.questions[ix] = new XormQuestionYesNo({
          ...question
        }).setOptions(ynOptions);
        break;
      case 'yes-no-dont':
        const yndOptions = [
          {key: 'Yes', value: 'Yes', jumpTo: ''},
          {key: 'No', value: 'No', jumpTo: ''},
          {key: 'I don\'t know', value: 'I don\'t know', jumpTo: ''}
        ];
        this.xormSection.questions[ix] = new XormQuestionYesNoDont({
          ...question
        }).setOptions(yndOptions);
        break;
      case 'single-choice':
        newQuestion = new XormQuestionSingleChoice({
          ...question
        });

        if (
          question instanceof XormQuestionWithOptions
            && !question.options.length
        ) {
          (newQuestion as XormQuestionSingleChoice).setOptions(question.options);
        }
        this.xormSection.questions[ix] = newQuestion;
        break;
      case 'multiple-choice':
        newQuestion = new XormQuestionMultipleChoice({
          ...question
        });

        if (
          question instanceof XormQuestionWithOptions
            && !question.options.length
        ) {
          (newQuestion as XormQuestionMultipleChoice).setOptions(question.options);
        }
        this.xormSection.questions[ix] = newQuestion;

        break;
      case 'single-choice-select':
        newQuestion = new XormQuestionSingleChoiceSelect({
          ...question
        });

        if (
          question instanceof XormQuestionWithOptions
            && !question.options.length
        ) {
          (newQuestion as XormQuestionSingleChoiceSelect).setOptions(question.options);
        }
        this.xormSection.questions[ix] = newQuestion;

        break;
      case 'date':

        break;
      case 'time':

        break;
      case 'datatable':
        this.xormSection.questions[ix]['rows'] = [];
        this.xormSection.questions[ix]['cols'] = [];
        this.xormSection.questions[ix]['settings'] = {
          'rows': {
            'none': true,
            'displayedLinesDefault': 1,
          },
          'cols': {
            'customTypes': false
          }
        };
        this.xormSection.questions[ix]['type'] = 'datatable';
        this.xormSection.questions[ix]['question'] = {
          'type': 'number'
        };

        break;
      case  'scale': 
        this.xormSection.questions[ix]['min'] = {
          text: 'Min',
          value: 0
        };
        this.xormSection.questions[ix]['max'] = {
          text: 'Max',
          value: 100
        };
        this.xormSection.questions[ix]['type'] = 'scale';

        break;
      case 'rate':
        this.xormSection.questions[ix]['count'] = {
          value: 5
        };
        this.xormSection.questions[ix]['type'] = 'rate';

      default:

        break;
    }

    console.log('[onTypechanged] ... ', this.xormSection.questions[ix]);
  }

  getAllPostOptions(key, itemType, optionsType) {
    const fullOptions = JSON.parse(JSON.stringify(this.allOptions));
    let section;

    if (itemType == 1) {
      section = key.split('__')[0];
    } else {
      section = key;
    }

    const j = 0;
    while (j < fullOptions.length && fullOptions[j].id != section) {
      fullOptions.splice(j, 1);
    }

    if (itemType == 0) {
      return fullOptions;
    }

    if (!fullOptions[0].options[0]) {
    }
    while (fullOptions[0].options[0] && fullOptions[0].options[0].id.key != key) {
      fullOptions[0].options.splice(0, 1);
    }
    fullOptions[0].options.splice(0, 1);

    return fullOptions;
  }

  getAllPreOptions() {
    const fullOptions = JSON.parse(JSON.stringify(this.allOptions));
    let section;

    fullOptions.splice(this.sectionIndex - 1);

    // Remove all ARRAY Section;
    let j = 0;
    while (j < fullOptions.length) {
      if (fullOptions[j].params.tarray) {
        fullOptions.splice(j, 1);
      } else {
        j++;
      }
    }
    return fullOptions;
  }

  dropOption(event: CdkDragDrop<string[]>, q:number) {
    moveItemInArray(this.xormSection.questions[q].options, event.previousIndex, event.currentIndex);
  }

  onAddOption(q: number) {

    this.xormSection.questions[q].options.push({
      key: 'Option ' + (this.xormSection.questions[q].options.length + 1)
    });
  }

  onAddRow(q: number) {
    this.xormSection.questions[q].rows.push({
      text: 'Row ' + this.xormSection.questions[q].rows.length + 1,
      header: false
    });
  }

  onRemoveRow(q:number, r: number) {
    let question = this.xormSection.questions[q];

    this.xormSection.questions[q].rows.splice(r, 1);
  }

  onAddCol(q: number) {
    this.xormSection.questions[q].cols.push({
      text: 'Col ' + this.xormSection.questions[q].cols.length + 1
    });
  }

  onRemoveCol(q: number, c: number) {
    let question = this.xormSection.questions[q];

    this.xormSection.questions[q].cols.splice(c, 1);
  }

  addSubchoiceToColumn(q: number, c: number) {
    console.log('addSubchoiceToColumn ', q, c);
    this.xormSection.questions[q].cols[c]['choices'].push({ value: '' });
  }

  removeSubchoiceToColumn(q: number, c: number, i: number) {
    this.xormSection.questions[q].cols[c].choices.splice(i, 1);
  }

  onColumnTypeChanged(newColumnType: string, q: number, c: number) {
    console.log('onColumnTypeChanged ', newColumnType, q, c);
    if (newColumnType === 'Single Choice') {
      this.xormSection.questions[q].cols[c]['choices'] = [];
    } else {
      this.xormSection.questions[q].cols[c].choices = null;
    }
  }

  onRemoveOption(q: number, o: number) {
    let question = this.xormSection.questions[q];
    if (question.options <= 1) {
      return;
    }

    this.xormSection.questions[q].options.splice(o, 1);
    if (question.options.length == 0 &&
        (question.type === 'radio' || question.type === 'checkbox' || question.type === 'dropdown')) {
      this.xormSection.questions[q].options.push({
        key: 'Option 1',
        jumpTo: ''
      });
    }
  }

  onAddOther(q: number) {
    this.xormSection.questions[q].otherOption = true;
    this.xormSection.questions[q].options.push({
      key: 'Other',
      isOther: true,
      enterValue: false
    });
  }

  onDuplicateQuestion(q: number) {
    this.duplicateQuestion.emit({
      sectionIndex: this.sectionIndex,
      question: q
    });
  }

  onDeleteQuestion(q: number) {
    this.deleteQuestion.emit({
      sectionIndex: this.sectionIndex,
      question: q
    });
    this.currentQuestion = +q - 1;
  }

}

