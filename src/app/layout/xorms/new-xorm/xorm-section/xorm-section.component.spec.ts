import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormSectionComponent } from './xorm-section.component';

describe('XormSectionComponent', () => {
  let component: XormSectionComponent;
  let fixture: ComponentFixture<XormSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
