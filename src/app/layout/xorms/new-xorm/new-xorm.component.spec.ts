import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewXormComponent } from './new-xorm.component';

describe('NewXormComponent', () => {
  let component: NewXormComponent;
  let fixture: ComponentFixture<NewXormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewXormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewXormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
