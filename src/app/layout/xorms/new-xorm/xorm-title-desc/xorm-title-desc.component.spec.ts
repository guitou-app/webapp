import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormTitleDescComponent } from './xorm-title-desc.component';

describe('XormTitleDescComponent', () => {
  let component: XormTitleDescComponent;
  let fixture: ComponentFixture<XormTitleDescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormTitleDescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormTitleDescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
