import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, multicast } from 'rxjs/operators';

import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';

import { SocketIoService } from '../../../_services/socket-io.service';
import { XormsService } from '../../../_services/xorms.service';
import { Project } from 'src/app/_models/project';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-all-xorms',
  templateUrl: './all-xorms.component.html',
  styleUrls: ['./all-xorms.component.less']
})
export class AllXormsComponent implements OnInit {

  allProjects: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private xormsService: XormsService,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    this.loadProjects();
  }

  loadProjects() {
    this.xormsService.fetchAll().pipe(first()).subscribe(
      (result:Project[]) => {
        console.log("Result... FORM ... ", result);
        this.allProjects = result;
        console.log(this.allProjects);
      }
    )
  }

  onGenerateAPK(xormId) {
    console.log("onGenerateAPK()", xormId);
    this.socketIoService.emitFormGeneration({ xormId: xormId }).pipe(first()).subscribe(
      (data) => {
        console.log(data);
      }
    )
  }

  createProject() {
    let modal = this.modalService.create({
      nzTitle: 'Create a new project',
      nzContent: NewModalXormComponent
    });

    modal.afterClose.subscribe(result => {
      this.router.navigate(['/projects/', result.id]);
    }); // console.log('[afterClose] The result is:', result));
  }

  deleteProject(id:string) {
    this.xormsService.delete(id).pipe(first()).subscribe(
      (result) => {
        this.allProjects = this.allProjects.filter(p => p.id !== id);
      },
      (err) => {
        console.error("Sorry, an error occurred. Try again, please.");
      }
    );
  }


}

@Component({
  selector: 'new-modal-xorm-component',
  template: `
    <form nz-form [formGroup]="createXormForm" class="login-form" (ngSubmit)="submitForm()">
      <nz-alert nzType="error" nzClosable="true" [hidden]="!error" [nzDescription]="error"></nz-alert>
      <nz-form-item>
        <nz-form-label nzfor="title" nzrequired="" class="ant-form-item-label">Title</nz-form-label>
        <nz-form-control nzErrorTip="Please input the title!">
          <nz-input-group>
            <input type="text" nz-input formControlName="title" placeholder="Please, enter the project title" />
          </nz-input-group>
        </nz-form-control>
      </nz-form-item>
      <nz-form-item>
        <nz-form-label nzfor="description" nzrequired="" class="ant-form-item-label">Description</nz-form-label>
        <nz-form-control nzErrorTip="Please describe it!">
          <nz-input-group>
            <textarea formControlName="description" nz-input rows="6" placeholder="Describe a bit your project"></textarea>
          </nz-input-group>
        </nz-form-control>
      </nz-form-item>
    </form>
    <div *nzModalFooter>
      <button nz-button nzType="primary" (click)="submitForm()">Create xorm</button>
    </div>
  `
})
export class NewModalXormComponent {
  error: string;

  createXormForm = new FormGroup({
		title: new FormControl('', Validators.required),
		description: new FormControl('', Validators.required)
	});

  constructor(
    private modal: NzModalRef,
    private xormsService: XormsService
  ) {}

  destroyModal(): void {
    this.modal.destroy();
  }

  submitForm() {
    console.log(this.createXormForm.value);
    this.xormsService.save(this.createXormForm.value).pipe(first()).subscribe(
      (result:any) => {
        console.log("Result... SAVE FORM ... ", result);
        this.modal.destroy(result);
      },
      (err) => {
        this.error = "Sorry, an error occurred. Try again, please."
      }
    );
  }
}
