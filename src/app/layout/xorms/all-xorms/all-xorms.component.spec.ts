import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllXormsComponent } from './all-xorms.component';

describe('AllXormsComponent', () => {
  let component: AllXormsComponent;
  let fixture: ComponentFixture<AllXormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllXormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllXormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
