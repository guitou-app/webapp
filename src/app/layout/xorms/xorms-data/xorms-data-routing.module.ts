import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { XormsDataComponent } from './xorms-data.component';
import { XormsAllDataComponent } from './xorms-all-data/xorms-all-data.component';
// import { XormsDataNewComponent } from './xorms-data-new/xorms-data-new.component';
import { XormsDataEditComponent } from './xorms-data-edit/xorms-data-edit.component';

const routes: Routes = [{
  path: '',
  component: XormsDataComponent,
  children: [
    { path: '', component: XormsAllDataComponent },
    // { path: 'new', component: XormsDataNewComponent },
    { path: ':dataId/edit', component: XormsDataEditComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XormsDataRoutingModule { }
