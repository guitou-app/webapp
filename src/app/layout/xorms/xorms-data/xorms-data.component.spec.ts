import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDataComponent } from './xorms-data.component';

describe('XormsDataComponent', () => {
  let component: XormsDataComponent;
  let fixture: ComponentFixture<XormsDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
