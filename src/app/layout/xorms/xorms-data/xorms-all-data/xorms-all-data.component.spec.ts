import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsAllDataComponent } from './xorms-all-data.component';

describe('XormsAllDataComponent', () => {
  let component: XormsAllDataComponent;
  let fixture: ComponentFixture<XormsAllDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsAllDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsAllDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
