import { XormsDataEditComponent } from './../xorms-data-edit/xorms-data-edit.component';
import { Component, OnInit, Input } from '@angular/core';
import {Location, JsonPipe} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { ColDef, ColumnApi, GridApi, GridOptions } from 'ag-grid-community';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { first } from 'rxjs/operators';
import * as _ from "lodash";

import { environment } from '../../../../../environments/environment';

import { DataService } from '../../../../_services/data.service';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-xorms-data',
  templateUrl: './xorms-all-data.component.html',
  styleUrls: ['./xorms-all-data.component.less']
})
export class XormsAllDataComponent implements OnInit {

  projectId: string;
  allDatas: any[] = [];
  allForms: any[] = [];

  selectedForm: string;
  currentForm: any = {};
  selectedColumns: string[] = [];
  selectedData: any[] = [];

  // settings:any = {};
  // data:any[] = [];
  originalData:any[] = [];
  defaultColDef:any = {
    sortable: true,
    filter: true,
    resizable: true,
    editable: true,
    // checkboxSelection: true,
  };
  gridOptions: GridOptions;

  firstColumn:any = {
    headerName: '#',
    width: 45,
    checkboxSelection: true,
    sortable: false,
    suppressMenu: true,
    pinned: true
  };

  // gridApi and columnApi
  private api: GridApi;
  private columnApi: ColumnApi;

  currentData:any = undefined;
  openSide:boolean = false;

  newDataUrl: string;

  constructor(
    private activatedRoute:ActivatedRoute,
    private modalService: NzModalService,
    private _location: Location,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.gridOptions = <GridOptions>{};

    this.projectId = this.activatedRoute.parent.parent.parent.snapshot.params['projectId'];
    if (this.projectId) {
      this._loadData(null);

      this.newDataUrl = `${environment.websiteUrl}/${this.projectId}`;
    }
  }

  _loadData(defaultForm) {
    this.dataService.fetchAll(this.projectId).pipe(first()).subscribe(
      (result:any) => {
        console.log(result);

        if (!result.success) {
          console.log("Error when retreiving stats information");

          return;
        }

        this.allForms = result.data.forms;
        this.allDatas = result.data.data;
        this.allDatas.map(data => {
          const xForm = this.allForms.filter(form => form._id === data.form)[0];

          Object.keys(data.values).forEach(section => {
            const xSection = xForm.form[section];
            if (xSection === undefined) {
              return;
            }

            Object.keys(xSection.questions).forEach((question, qix) => {
              const xQuestion = xSection.questions[question];

              if (xQuestion.type === 'multiple-choice') {
                // console.log('MULTIPLE ... ', xQuestion.kv); //242394288

                if (xSection._params.repeat) {
                  for (let i=0; i < data.values[section].length; i++) { //} data.values[section]) {
                    if (!Array.isArray(data.values[section][i][question])) {
                      const v = data.values[section][i][question];
                      data.values[section][i][question] = v ? v.replace('[', '').replace(']', '').split(', ') : [];
                    }

                    Object.keys(xQuestion.kv).forEach((key, kix) => {
                      const value = xQuestion.kv[key];

                      if (data.values[section][i][question].indexOf(value) > -1) {
                        data.values[section][i][`${question}__${key}`] = 1;
                      } else {
                        data.values[section][i][`${question}__${key}`] = 0;
                      }
                    });
                  }
                } else {
                  if (!Array.isArray(data.values[section][question])) {
                    const v = data.values[section][question];
                    data.values[section][question] = v ? v.replace('[', '').replace(']', '').split(', ') : [];
                  }

                  Object.keys(xQuestion.kv).forEach((key, kix) => {
                    const value = xQuestion.kv[key];

                    if (data.values[section][question].indexOf(value) > -1) {
                      data.values[section][`${question}__${key}`] = 1;
                    } else {
                      data.values[section][`${question}__${key}`] = 0;
                    }
                  });
                }
              }

              if (xQuestion.type === 'datatable') {
                if (!xQuestion.settings.rows.exists) {
                  
                  data.values[section][`${question}__count`] = data.values[section][question].length;
                }
              }
            });
          });

          return data;
        });

        this.originalData = JSON.parse(JSON.stringify(this.allDatas));

        if (!defaultForm) {
          this.selectedForm = this.allForms[0]._id;
        } else {
          this.selectedForm = defaultForm;
        }
        this.onSelectedFormChanged();
      }
    );
  }

  onSelectedFormChanged() {
    this.currentForm = this.allForms.filter(form => form._id === this.selectedForm)[0];
    this.selectedData = this.originalData.filter(datum => datum.form === this.selectedForm).map(datum => {
      let values = {};
      Object.keys(datum.values).forEach(section => {
        if (Array.isArray(datum.values[section])) {
          const item = {};
          item[`${section}`] = datum.values[section].length;
          values = {...values, ...item};
        } else {

          values = {...values, ...datum.values[section]} ;
        }
      });
      values['id'] = datum._id;
      values['Date'] = datum.createdAt;
      values['Key'] = datum.primaryKey;
      values['IP'] = 'location' in datum ? datum.location.ip : '';
      values['Coords'] = 'location' in datum  && 'coords' in datum.location ? datum.location.coords.longitude + ", " + datum.location.coords.latitude : '';
      values['Author'] = datum.values['section_final'] ? datum.values['section_final']['section_final__name'] : '';

      return values;
    });

    let variables = [];
    Object.keys(this.currentForm.form).forEach(section => {
      const xSection = this.currentForm.form[section];
      let questions = [];

      if (!xSection.questions) {
        return null;
      }

      if (xSection._params.repeat) {
        questions = [
          {
            section,
            headerName: `${section}`,
            headerNameTitle: `Number of items ${section}`,
            field: `${section}`,
            width: 175,
            editable: false,
          }
        ];
      } else {

        questions = Object.keys(xSection.questions).map(quest => {
          const item = xSection.questions[quest];

          if (item.type === 'datatable') {
            const items = [];

            if (item.settings.rows.exists) {
              for (let c = 0; c < item.cols.length; c++) {
                for (let r = 0; r < item.rows.length; r++) {
                  const variable = `${quest}__row_${r}__col_${c}`;
                  const column = {
                    section,
                    headerName: variable,
                    field: variable,
                    width: 175
                  };
  
                  items.push(column);
                }
              }
            } else {
              const column = {
                section,
                headerName: `${quest}__count`,
                field: `${quest}__count`,
                width: 175,
                editable: false,
              };

              items.push(column);
            }

            return items;
          } else if (item.type === 'multiple-choice') {
            const items = [];
            items.push({
              section,
              headerName: quest,
              field: quest,
              width: 175
            });

            Object.keys(item.kv).forEach(key => {
              items.push({
                section,
                headerName: `${quest}__${key}`,
                field: `${quest}__${key}`,
                width: 175
              });
            });

            return items;
          } else {

            return {
              section,
              headerName: quest,
              field: quest,
              width: 175
            };
          }

       });

      }

      variables = [...variables, ...questions.reduce((acc, val) => acc.concat(val), [])];
    });

    variables.unshift({
      section: '0000',
      headerName: 'Author',
      field: 'Author',
      width: 200,
      sortable: true,
    });
    variables.unshift({
      section: '0000',
      headerName: 'Key',
      field: 'Key',
      width: 200,
      sortable: true,
    });
    variables.unshift({
      section: '0000',
      headerName: 'Date',
      field: 'Date',
      width: 200,
      sortable: true,
    });
    variables.unshift({
      section: '0000',
      headerName: 'Coords',
      field: 'Coords',
      width: 200,
      sortable: true,
    });
    variables.unshift({
      section: '0000',
      headerName: 'IP',
      field: 'IP',
      width: 200,
      sortable: true,
    });
    variables.unshift(this.firstColumn);
    this.selectedColumns = [...variables];
  }

  onGridReady(params): void {
    this.api = params.api;
    this.columnApi = params.columnApi;

    this.api.sizeColumnsToFit();

    // temp fix until AG-1181 is fixed
    this.api.hideOverlay();
  }

  onRowClicked(params): void {
    console.log("onRowClick", params);
    this.currentData = this.originalData.find(d => d._id === params.data.id);
    // this.openSide = true;
    // console.log(this.currentData);
  }

  onRowValueChanged(event): void {
    console.log("onRowValueChanged", event);
  }

  onSelectionChanged(params): void {
    console.log(params.api.getSelectedRows());
    // this.currentData = params.api.getSelectedRows()[0];
    // this.openSide = true;
    // console.log()
  }

  onCellClick(event) {
    console.log("onCellClick", event);
  }

  onCellDoubleClick(event) {
    console.log("onCellDoubleClick", event);
    const colDef = event.colDef;
    let [section, question, ...rest] = colDef.field.split('__');
    console.log(section, question, rest);
    question = `${section}__${question}`;

    let data = [];
    let columns = [];

    if (this.currentForm.form[section]._params.repeat) {
      console.log('repeated section', section);
      data = this.currentData.values[section];
      columns = Object.keys(this.currentForm.form[section].questions).map((q) => {
        return {
          headerName: q,
          field: q,
          width: 175,
          editable: true,
        };
      });
    } else {
      console.log('repeated not section', section);
      const q = this.currentForm.form[section].questions[question]
      if (  
        q.type === 'datatable' &&
        !q.settings.rows.exists
      ) {
        console.log('row not existed', q);
        data = this.currentData.values[section][question];
        columns = [];
        for (let j=0; j < q.cols.length; j++) {
          // columns.push(`${question}__col_${j}`);
          columns.push({
            headerName: `${question}__col_${j}`,
            field: `${question}__col_${j}`,
            width: 175,
            editable: true,
          });
        }
      } else {
        return;
      }
    }

    const modal = this.modalService.create({
      nzTitle: `${colDef.field} - Data`,
      nzContent: XormsRepeatedDataComponent,
      nzComponentParams: {
        section: colDef.field,
        data: data,
        index: event.rowIndex,
        columns: columns
      },
      nzFooter: null
    });

    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));

    // Return a result when closed
    modal.afterClose.subscribe(result => {
      console.log('[afterClose] The result is:', result);
      if (result) {
        const { toSave, data } = result;
        if (toSave) {
          this.currentData.values[colDef.field] = data;

          this.onUpdateCurrentData();
        }
      }
    });
  }

  onUpdateCurrentData() {
    console.log('onUpdateCurrentData', this.currentData);
  }

  toggleOpenSide() {
    this.openSide = !this.openSide;
  }

  rowsSelected() {
    return this.api && this.api.getSelectedRows().length > 0;
  }

  goBack() {
    this._location.back();
  }

  openDeleteModal() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this data?',
      nzContent: '<b style="color: red;">Some descriptions</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        console.log('OK');
        this.dataService.delete(this.projectId, this.currentData._id).pipe(first()).subscribe(
          (result:any) => {
            console.log("Delete result........");
            console.log(result);
            this._loadData(null);
          }
        );
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  confirmDelete(): void {
    this.dataService.delete(this.currentData.form, this.currentData._id).pipe(first()).subscribe(
      (result:any) => {
        console.log("Delete result........");
        console.log(result);
        this._loadData(null);
      }
    );
    // this.modalRef.hide();
  }

  decline(): void {
    // this.message = 'Declined!';
    // this.modalRef.hide();
  }

  onAddData() {
    console.log("onAddData", this.currentData, this.currentForm);
    const modal = this.modalService.create({
      nzTitle: `Add Data`,
      nzContent: XormsDataEditComponent,
      nzWidth: '90%',
      nzComponentParams: {
        currentData: {},
        // currentFormModel: this.currentForm,
        projectId: this.projectId,
        xormId: this.currentForm.id,
        add: true
      },
      nzFooter: null
    });

    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));

    // Return a result when closed
    modal.afterClose.subscribe(result => {
      console.log('[afterClose] The result is:', result);

      if (!result) {
        return;
      }

      if (result.success) {
        this._loadData(this.selectedForm);
      }
    });
  }

  onEditData() {
    console.log("onEditData", this.currentData);
    const modal = this.modalService.create({
      nzTitle: `Edit Data`,
      nzContent: XormsDataEditComponent,
      nzWidth: '90%',
      nzComponentParams: {
        currentData: this.currentData,
        currentFormModel: this.currentForm
      },
      nzFooter: null
    });

    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));

    // Return a result when closed
    modal.afterClose.subscribe(result => {
      console.log('[afterClose] The result is:', result);

      if (!result) {
        return;
      }

      if (result.success) {
        this._loadData(this.selectedForm);
      }
    });
  }

  onExcelExport() {
    let filename = `${this.currentForm.title}`;
    const xSelectedData = this.selectedData.map(d => {
      const datum = {};

      Object.keys(d).forEach(key => {
        const value = d[key];
        if (Array.isArray(value)) {
          datum[key] = value.join(', ');
        } else {
          datum[key] = value;
        }
      });

      return datum;
    });

    console.log(xSelectedData);
    console.log(XLSX.utils.json_to_sheet(xSelectedData));
    let worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(xSelectedData);
    const workbook: XLSX.WorkBook = { Sheets: { Questionnaire: worksheet }, SheetNames: ['Questionnaire'] };

    const datas = {};
    this.originalData.filter(datum => datum.form === this.selectedForm).map(datum => {
      let qID;
      Object.keys(datum.values).forEach((section, index) => {
        if (index === 0) {
          qID = datum.values[section]['Q00'];
        }

        if (Array.isArray(datum.values[section])) {
          if (!datas[section]) {
            datas[section] = [];
          }

          for (let i = 0; i < datum.values[section].length; i++) {
            datum.values[section][i]['ID'] = qID;

            Object.keys(datum.values[section][i]).forEach(key => {
              const value = datum.values[section][i][key];
              if (Array.isArray(value)) {
                datum.values[section][i][key] = value.join(', ');
              }
            });
          }
          // datum.values[section]['ID'] = qID;
          datas[section].push(...datum.values[section]);
        }
      });
    });
    Object.keys(datas).forEach(section => {
      worksheet = XLSX.utils.json_to_sheet(datas[section]);
      workbook.Sheets[section] = worksheet;
      workbook.SheetNames.push(section);
    });

    const dictionnary = [];
    const choices = [];
    Object.keys(this.currentForm.form).forEach(section => {
      const xSection = this.currentForm.form[section];
      Object.keys(xSection.questions).forEach(question => {
        const xQuestion = xSection.questions[question];

        let type;
        if (['text', 'string'].indexOf(xQuestion.type) > -1) {
          type = 'text';
        } else if (xQuestion.type === 'multiple-choice') {
          type = 'multiple choices';
        } else if (['single-choice-select', 'yes-no', 'yes-no-dont', 'single-choice'].indexOf(xQuestion.type) > -1) {
          type = 'one choice';
        } else {
          type = xQuestion.type;
        }

        dictionnary.push({
          Variable: question,
          Type: type,
          Question: xSection.questions[question].title,
        });

        if (xQuestion.type === 'datatable') {
          for (let c = 0; c < xQuestion.cols.length; c++) {
            for (let r = 0; r < xQuestion.rows.length; r++) {
              const variable = `${question}__row_${r}__col_${c}`;

              choices.push({
                Variable: variable,
                Label: `${xQuestion.rows[r]} - ${xQuestion.cols[c]}`,
                Value: ''
              });
            }
          }
        } else if (['multiple-choice', 'single-choice-select', 'yes-no', 'yes-no-dont', 'single-choice'].indexOf(xQuestion.type) > -1) {
          Object.keys(xQuestion.kv).forEach(key => {
            choices.push({
              Variable: question,
              Label: xQuestion.kv[key],
              Value: key
            });
          });
        }
      });
    });
    worksheet = XLSX.utils.json_to_sheet(dictionnary);
    workbook.Sheets['Dictionnary'] = worksheet;
    workbook.SheetNames.push('Dictionnary');
    worksheet = XLSX.utils.json_to_sheet(choices);
    workbook.Sheets['Choices'] = worksheet;
    workbook.SheetNames.push('Choices');

    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, filename);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

}

@Component({
  selector: 'app-xorms-data-repeated-section-data',
  template: `
    <div>
      <ag-grid-angular
        style="height:300px"
        class="ag-theme-balham w-100"

        (gridReady)="onGridReady($event)"
        [gridOptions]="gridOptions"
        (rowDataChanged)="onRowDataChanged($event)"
        (cellValueChanged)="onCellValueChanged($event)"

        [rowData]="data"
        [columnDefs]="columns"

        [defaultColDef]="defaultColDef"
      >
      </ag-grid-angular>
      <div>
        <button nz-button nzType="default" (click)="onCancel()">Cancel</button>
        <button nz-button nzType="primary" (click)="onSave()">Save</button>
      </div>
    </div>
  `
})
export class XormsRepeatedDataComponent implements OnInit {
  @Input() section: string;
  @Input() data: any[] = [];
  @Input() index = -1;
  @Input() columns: any[] = [];

  defaultColDef:any = {
    sortable: true,
    filter: true,
    resizable: true,
    editable: true,
    // checkboxSelection: true,
  };
  gridOptions: GridOptions;

  firstColumn:any = {
    headerName: '#',
    width: 45,
    checkboxSelection: true,
    sortable: false,
    suppressMenu: true,
    pinned: true
  };

  // gridApi and columnApi
  private api: GridApi;
  private columnApi: ColumnApi;

  toSave = false;

  constructor(private modal: NzModalRef) {}

  ngOnInit() {
    console.log(this.columns);
    console.log(this.data);
  }

  onGridReady(params): void {
    this.api = params.api;
    this.columnApi = params.columnApi;

    this.api.sizeColumnsToFit();

    // temp fix until AG-1181 is fixed
    this.api.hideOverlay();
  }

  onRowDataChanged(event) {
    console.log("onRowDataChanged", event);
  }

  onCellValueChanged(event) {
    console.log("onCellValueChanged", event);
    this.toSave = true;
    this.data[event.rowIndex] = event.data;
    console.log(this.data);
  }

  onSave(): void {
    this.modal.destroy({
      toSave: this.toSave,
      data: this.data,
      index: this.index
    });
  }

  onCancel(): void {
    this.modal.destroy();
  }
}
