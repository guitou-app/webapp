import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared/shared.module';

import { XormsDataRoutingModule } from './xorms-data-routing.module';
import { XormsDataComponent } from './xorms-data.component';
import { XormsAllDataComponent, XormsRepeatedDataComponent } from './xorms-all-data/xorms-all-data.component';
import { XormsDataNewComponent } from './xorms-data-new/xorms-data-new.component';
import { XormsDataEditComponent } from './xorms-data-edit/xorms-data-edit.component';

@NgModule({
  declarations: [
    XormsDataComponent,
    XormsAllDataComponent, XormsRepeatedDataComponent,
    XormsDataNewComponent,
    XormsDataEditComponent
  ],
  imports: [
    CommonModule,
    XormsDataRoutingModule,
    FormsModule, ReactiveFormsModule,

    SharedModule
  ],
  entryComponents: [
    XormsRepeatedDataComponent
  ]
})
export class XormsDataModule { }
