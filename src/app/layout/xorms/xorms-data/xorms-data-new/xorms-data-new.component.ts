import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';

import { first } from 'rxjs/operators';

import { FormsService } from '../../../../_services/forms.service';
import { DataService } from '../../../../_services/data.service';

@Component({
  selector: 'app-xorms-data-new',
  templateUrl: './xorms-data-new.component.html',
  styleUrls: ['./xorms-data-new.component.less']
})
export class XormsDataNewComponent implements OnInit {

  projectId: string;
  xormId:string;
  oldValue:any = {};
  formModel:any = {};
  currentFormModel: any = {};
  currentFormControls: FormGroup = this.formBuilder.group({
    "stay_here": false
  });;

  agridData:any = {
    data: {},
    columns: {}
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private formsService: FormsService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    console.log(this.router);
    console.log(this.activatedRoute.snapshot.params);
    this.projectId = this.activatedRoute.parent.snapshot.params['projectId'] || this.activatedRoute.snapshot.params['projectId'];
    this.xormId = this.activatedRoute.parent.snapshot.params['xormId'] || this.activatedRoute.snapshot.params['xormId'];

    console.log("Loof Fooomm ", this.xormId);
    if (this.xormId) {
      this._loadForm();
    }
  }

  _loadForm() {
    // this.formsService.fetchOne(this.xormId).pipe(first()).subscribe(
    //   (result:any) => {
    //     console.log(result);

    //     if (!result.success) {
    //       console.log("Error when retreiving stats information");

    //       return;
    //     }

    //     this.currentFormModel = result.data;
    //     this.formModel = this.currentFormModel.form;
    //     console.log(this.currentFormModel.form);
    //     this._createFormControls();
    //   }
    // )
  }

  _createFormControls() {
    Object.keys(this.currentFormModel.form).forEach(section => {
      let sectionFormGroup = this.formBuilder.group({});
      console.log(this.currentFormModel.form[section].questions);
      if (!this.currentFormModel.form[section].questions) {
        this.currentFormModel.form[section].questions = {}
      }

      let columns = [];
      Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
        let type:any = this.currentFormModel.form[section].questions[question];

        columns.push({
          headerName: type.title,
          field: question
        });
        sectionFormGroup.addControl(question, this.formBuilder.control(''));
      });

      if (this.currentFormModel.form[section].hasOwnProperty('_params')) {
        if (this.currentFormModel.form[section]._params.tarray) {
          let arr = this.formBuilder.array([]);
          arr.push(sectionFormGroup)

          this.currentFormControls.addControl(section, arr);

          this.agridData.columns[section] = columns;
          this.agridData.data[section] = [];
        } else {
          this.currentFormControls.addControl(section, sectionFormGroup);
        }
      } else {
        this.currentFormControls.addControl(section, sectionFormGroup);
      }
    });
  }

  getFromGroup(section) {
    return this.currentFormControls.get(section);
  }

  getSectionRepetitionLimit(section_key) {
    let section = this.currentFormModel.form[section_key];
    if (section._params.tarray_max_times == 'fixed') {
      if (section._params.tarray_max_times_fixed == 0) {
        return 'No limit';
      } else {
        return this.currentFormModel.form[section_key]._params.tarray_max_times_fixed - (this.currentFormControls.value[section_key].length - 1);
      }
    }

    if (section._params.tarray_max_times == 'variable') {
      let variable = section._params.tarray_max_times_variable;
      let variable_value = this.currentFormControls.value[variable.split('__')[0]][variable];

      return variable_value - (this.currentFormControls.value[section_key].length - 1);
    }
  }

  seePreviousValues(sectionKey) {
    let values = JSON.parse(JSON.stringify(this.currentFormControls.value[sectionKey]));
    values.pop();
    this.agridData.data[sectionKey] = values;
    return values;
  }

  onAddValue(section) {
    console.log("onAddValue -- ", section);
    let sectionFormGroup = this.formBuilder.group({});

    Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
      sectionFormGroup.addControl(question, this.formBuilder.control(''));
    });

    (<FormArray>this.currentFormControls.get(section)).push(sectionFormGroup);
  }

  onAddAndNext(section) {
    console.log("onAddAndNext -- ", section);
  }

  onSubmit() {
    console.log("onSubmit()");
    let values = this.currentFormControls.value;
    let stayHere = values['stay_here'];
    delete values['stay_here'];

    let datum =  {
      values: values,
      projectId: this.projectId,
      xormId: this.xormId,
      origin: 'web'
    };
    console.log(datum);
    console.log(values);
    console.log(stayHere);

    this.dataService.save(this.xormId, datum).pipe(first()).subscribe(
      (result:any) => {
        console.log(result);

        if (!result.success) {
          console.log("Error when retreiving stats information");

          return;
        }

        if (!stayHere) {
          this.router.navigate(['/xorms/', this.xormId, 'data']);
        }
        this.currentFormControls.reset();
        this.currentFormControls

      }
    );
  }

  selectionChangeOnSingle($event, oldValue) {
    console.log($event);
    let source = {
      section: $event.target.id.split('__')[0],
      question: $event.target.id,
      oldValue: oldValue,
      currentValue: undefined
    }
    source["currentValue"] = this.currentFormControls.value[source.section][source.question]; // $event.target.id.split('__')[1]

    console.log(source);
    console.log(this.currentFormModel.form[source.section]);
    console.log(this.currentFormModel.form[source.section].questions[source.question]);

    if ('jumpTo' in this.currentFormModel.form[source.section].questions[source.question]) {
      console.log("IN JUMP TO...");
      console.log(this.currentFormControls.value);
      console.log(source);



      if(source.oldValue && source.oldValue in this.currentFormModel.form[source.section].questions[source.question].jumpTo) {
        let jumpToId = this.currentFormModel.form[source.section].questions[source.question].jumpTo[source.oldValue];
        console.log("OLD VALUE", jumpToId);
        console.log(oldValue);

        // if (jump)
        let toId = {
          section: jumpToId.split('__')[0],
          question: jumpToId
        }

        this._enableElement(source, toId);
      }

      if(source.currentValue && source.currentValue in this.currentFormModel.form[source.section].questions[source.question].jumpTo) {
        let jumpToId = this.currentFormModel.form[source.section].questions[source.question].jumpTo[source.currentValue];
        console.log("CURRENT VALUE", jumpToId);
        let toId = {
          section: jumpToId.split('__')[0],
          question: jumpToId
        }

        this._disableElement(source, toId);
      }
      // if ()
    }
  }

  _disableElement(fromQt, toQt) {
    let allCurrentValues = this.currentFormControls.value;
    let allFromQuestionsKeys = Object.keys(this.currentFormModel.form[fromQt.section].questions); // Object.keys(allCurrentValues[fromQt.section]);
    let fromIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    let j = fromIndex + 1;
    while (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] != toQt.question) {
      // Disable
      if (this.currentFormControls.get(fromQt.section) instanceof FormGroup) {
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).reset();
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).disable();
      }
      j++;
    }

    if (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] == toQt.question) {
      // We reach the targetQuestion
      console.log(this.renderer.selectRootElement(`#${toQt.question}`));
      this.renderer.selectRootElement(`#${toQt.question}`).focus();
      return;
    }

    // Disable all element between the Section
    let allSectionKeys = Object.keys(this.currentFormModel.form);
    j = 1 + allSectionKeys.indexOf(fromQt.section)
    while (j < allSectionKeys.length && allSectionKeys[j] != toQt.section) {
      // Disable all the section
      if (this.currentFormControls.get(allSectionKeys[j]) instanceof FormGroup) {
        this.currentFormControls.get(allSectionKeys[j]).disable();
      }
    }

    // Disable the elements,into the last section, that come before the jumpTo element
    let allToQuestionsKeys = Object.keys(this.currentFormModel.form[toQt.section].questions);
    let toIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    j = 0;
    while (j < toIndex) {
      // Disable
      if (this.currentFormControls.get(toQt.section) instanceof FormGroup) {
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).reset();
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).disable();
      }
      j++;
    }

    this.renderer.selectRootElement(`#${toQt.question}`).focus();
  }

  _enableElement(fromQt, toQt) {
    let allCurrentValues = this.currentFormControls.value;
    console.log(this.currentFormModel.form[fromQt.section]);
    let allFromQuestionsKeys = Object.keys(this.currentFormModel.form[fromQt.section].questions);
    let fromIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    let j = fromIndex + 1;
    console.log(allCurrentValues, allFromQuestionsKeys, fromIndex);
    while (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] != toQt.question) {
      console.log(allFromQuestionsKeys[j]);
      // Enable
      if (this.currentFormControls.get(fromQt.section) instanceof FormGroup) {
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).reset();
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).enable();
      }
      j++;
    }

    if (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] == toQt.question) {
      // We reach the targetQuestion
      // this.renderer.selectRootElement(`#${toQt.question}`).focus();
      return;
    }

    // Enable all element between the Section
    let allSectionKeys = Object.keys(this.currentFormModel.form);
    j = 1 + allSectionKeys.indexOf(fromQt.section)
    while (j < allSectionKeys.length && allSectionKeys[j] != toQt.section) {
      // Disable all the section
      if (this.currentFormControls.get(allSectionKeys[j]) instanceof FormGroup) {
        this.currentFormControls.get(allSectionKeys[j]).enable();
      }
    }

    // Enable the elements,into the last section, that come before the jumpTo element
    let allToQuestionsKeys = Object.keys(this.currentFormModel.form[toQt.section].questions);
    let toIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    j = 0;
    while (j < toIndex) {
      // Disable
      if (this.currentFormControls.get(toQt.section) instanceof FormGroup) {
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).enable();
      }
      j++;
    }
  }

}
