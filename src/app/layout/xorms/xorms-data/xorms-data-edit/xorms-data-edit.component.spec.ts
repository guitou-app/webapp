import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsDataEditComponent } from './xorms-data-edit.component';

describe('XormsDataEditComponent', () => {
  let component: XormsDataEditComponent;
  let fixture: ComponentFixture<XormsDataEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsDataEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsDataEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
