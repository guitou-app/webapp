import { Component, OnInit, AfterViewInit, AfterViewChecked, Renderer2, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';

import { first } from 'rxjs/operators';

import { FormsService } from '../../../../_services/forms.service';
import { DataService } from '../../../../_services/data.service';
import { variable } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-xorms-data-edit',
  templateUrl: './xorms-data-edit.component.html',
  styleUrls: ['./xorms-data-edit.component.less']
})
export class XormsDataEditComponent implements OnInit {
  @Input() add = false;
  @Input() currentData: any = {};
  @Input() projectId: string;
  @Input() xormId: string;
  currentFormModel: any = {};

  currentFormControls: FormGroup = new FormGroup({}); // this.formBuilder.group({});;
  isOK = false;
  isLoading = false;
  currentGroupRepeat: any = {};
  primaryKey = new FormControl('');

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private router: Router,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private modalRef: NzModalRef,
    private message: NzMessageService,
    private formsService: FormsService,
  ) { }

  ngOnInit() {
    console.log(this.projectId, this.xormId);

    this._loadForm();
    console.log(this.currentData);

  }

  _loadForm() {
    this.formsService.fetchOne(this.projectId, this.xormId).pipe(first()).subscribe(
      (result:any) => {

        if (!result.success) {

          return;
        }
        console.log(result);
        this.currentFormModel = result.data;

        if (this.add) {
          this._initFormControls();
        } else {
          this._createFormControls();
        }
      }
    );
  }

  showPrimary() {
    return (this.currentFormModel.level == 'primary' && this.currentFormModel.pKeyOrigin == 'entered') 
              || this.currentFormModel.level == 'secondary'
  }

  _initFormControls() {
    Object.keys(this.currentFormModel.form).forEach(section => {

      if (this.currentFormModel.form[section]._params.repeat) {
        const arr = this.formBuilder.array([]);
        const group = {};
        Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
          const currentQuestion = this.currentFormModel.form[section].questions[question];
          if (currentQuestion.type === 'multiple-choice') {
            group[question] = [[]]; // []; // ''; // [''];
          } else {
            group[question] = '';
          }
          // group[question] = '';
        });

        arr.push(this.formBuilder.group(group));
        // for (let i=0; i<this.currentData.values[section].length; i++) {
        //   const group = {};

        //   // console.log(this.currentData.values[section][i]);
          // Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
          //   const currentQuestion = this.currentFormModel.form[section].questions[question];
          //   if (currentQuestion.type === 'multiple-choice') {
          //     group[question] = [''];
          //   } else {
          //     group[question] = '';
          //   }
          // });

        //   arr.push(this.formBuilder.group(group));
        // }

        this.currentFormControls.addControl(section, arr);
        this.currentGroupRepeat[section] = 0;
      } else {
        const sectionFormGroup = this.formBuilder.group({});

        Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
          const currentQuestion = this.currentFormModel.form[section].questions[question];

          if (currentQuestion.type === 'datatable') {
            // for (let c = 0; c < currentQuestion.cols.length; c++) {
            //   for (let r = 0; r < currentQuestion.rows.length; r++) {
            //     const variable = `${question}__row_${r}__col_${c}`;
            //     sectionFormGroup.addControl(
            //       variable,
            //       this.formBuilder.control('')
            //     );
            //   }
            // }
            if (currentQuestion.settings.rows.exists) {
              for (let c=0; c < currentQuestion.cols.length; c++) {
                for (let r=0; r < currentQuestion.rows.length; r++) {
                  const variable = `${question}__row_${r}__col_${c}`;
                  sectionFormGroup.addControl(
                    variable,
                    this.formBuilder.control('')
                  );
                }
              }
            } else {
              const arr = this.formBuilder.array([]);
              const group = {}
              for (let j=0; j < currentQuestion.cols.length; j++) {
                const variable = `${question}__col_${j}`; //`${question}__row_${i}__col_${j}`;
                group[variable] = '';
              }
              arr.push(this.formBuilder.group(group));

              // for (let i=0; i < this.currentData.values[question].length + 1; i++) {
              //   const group = {}

              //   // Object.keys(this.currentFormModel.form[section][question])
              //   for (let j=0; j < currentQuestion.cols.length; j++) {
              //     const variable = `${question}__col_${j}`; //`${question}__row_${i}__col_${j}`;
              //     group[variable] = this.currentData.values[section][question][i] || ''
              //   }

              //   arr.push(this.formBuilder.group(group));
              // }

              sectionFormGroup.addControl(question, arr);
              console.log(arr);
            }
          } if (currentQuestion.type === 'multiple-choice') {
            sectionFormGroup.addControl(
              question,
              this.formBuilder.control([[]])
            );
          } else {
            sectionFormGroup.addControl(
              question,
              this.formBuilder.control('')
            );
          }
        });
        this.currentFormControls.addControl(section, sectionFormGroup);
      }
    });
    console.log(this.currentFormControls);
  }

  _createFormControls() {
    Object.keys(this.currentFormModel.form).forEach(section => {

      if (this.currentFormModel.form[section]._params.repeat) {
        const arr = this.formBuilder.array([]);

        for (let i=0; i<this.currentData.values[section].length; i++) {
          const group = {};

          // console.log(this.currentData.values[section][i]);
          Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
            const currentQuestion = this.currentFormModel.form[section].questions[question];
            if (currentQuestion.type === 'multiple-choice') {
              group[question] = [this.currentData.values[section][i][question] || ''];
            } else {
              group[question] = this.currentData.values[section][i][question] || '';
            }
          });

          arr.push(this.formBuilder.group(group));
        }

        this.currentFormControls.addControl(section, arr);
        this.currentGroupRepeat[section] = 0;
      } else {
        const sectionFormGroup = this.formBuilder.group({});

        Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
          const currentQuestion = this.currentFormModel.form[section].questions[question];
          console.log(currentQuestion.type);
          if (currentQuestion.type === "datatable") {
            console.log(currentQuestion.settings.rows.exists);

            if (currentQuestion.settings.rows.exists) {
              for (let c=0; c < currentQuestion.cols.length; c++) {
                for (let r=0; r < currentQuestion.rows.length; r++) {
                  const variable = `${question}__row_${r}__col_${c}`;
                  sectionFormGroup.addControl(
                    variable,
                    this.formBuilder.control(this.currentData.values[section][variable] || '')
                  );
                }
              }
            } else {
              const arr = this.formBuilder.array([]);

              for (let i=0; i < this.currentData.values[question].length + 1; i++) {
                const group = {}

                // Object.keys(this.currentFormModel.form[section][question])
                for (let j=0; j < currentQuestion.cols.length; j++) {
                  const variable = `${question}__col_${j}`; //`${question}__row_${i}__col_${j}`;
                  group[variable] = this.currentData.values[section][question][i] || ''
                }

                arr.push(this.formBuilder.group(group));
              }

              sectionFormGroup.addControl(question, arr);
              console.log(arr);
            }

          } else {
            sectionFormGroup.addControl(
              question,
              this.formBuilder.control(this.currentData.values[section][question] || '')
            );
          }
        });
        this.currentFormControls.addControl(section, sectionFormGroup);
      }
    });
    console.log(this.currentFormControls);
  }

  getFromGroup(section): FormGroup {
    return <FormGroup>this.currentFormControls.get(section);
  }

  getTableInputFormGroup(section, question): FormGroup {
    return <FormGroup>this.currentFormControls.get(section).get(question);
  }

  getSectionRepetitionLimit(section_key) {
    let section = this.currentFormModel.form[section_key];
    if (section._params.tarray_max_times == 'fixed') {
      if (section._params.tarray_max_times_fixed == 0) {
        return 'No limit';
      } else {
        return this.currentFormModel.form[section_key]._params.tarray_max_times_fixed - (this.currentFormControls.value[section_key].length - 1);
      }
    }

    if (section._params.tarray_max_times == 'variable') {
      let variable = section._params.tarray_max_times_variable;
      let variable_value = this.currentFormControls.value[variable.split('__')[0]][variable];

      return variable_value - (this.currentFormControls.value[section_key].length - 1);
    }
  }

  seePreviousValues(sectionKey) {
    let values = JSON.parse(JSON.stringify(this.currentFormControls.value[sectionKey]));
    values.pop();
    return values;
  }

  onNextGroup(section_key) {
    this.currentGroupRepeat[section_key] += 1;
    console.log(this.currentGroupRepeat);
  }

  onPrevGroup(section_key) {
    this.currentGroupRepeat[section_key] -= 1;
    console.log(this.currentGroupRepeat);
  }

  onAddTableInputRow(section, question) {
    let group = {};

    for (let j=0; j < this.currentFormModel.form[section].questions[question].cols.length; j++) {
      const variable = `${question}__col_${j}`;
      group[variable] = '';
    }

    (<FormArray>(this.currentFormControls.get(section).get(question))).push(this.formBuilder.group(group));
  }

  // onAddValue(section) {
  onAddNewGroup(section) {
    let sectionFormGroup = this.formBuilder.group({});

    Object.keys(this.currentFormModel.form[section].questions).forEach(question => {
      sectionFormGroup.addControl(question, this.formBuilder.control(''));
    });

    (<FormArray>this.currentFormControls.get(section)).push(sectionFormGroup);
    this.currentGroupRepeat[section] += 1;
  }

  onAddAndNext(section) {
    console.log("onAddAndNext -- ", section);
  }

  onSubmit() {
    console.log("onSubmit()");
    this.isLoading = true;
    const values = this.currentFormControls.value;
    if (!this.add) {
      values["section_final"] = this.currentData.values["section_final"];
    }

    let datum =  {
      ...this.currentData,
      primaryKey: this.showPrimary() ? this.primaryKey.value : '',
      values, //: {, ...this.currentData.values["section_final"] },
      // form: this.xormId,
      provider: 'web'
    };
    console.log(datum);
    console.log(this.router.url);
    console.log(window.location.href);
    // return;

    if (this.add) {
      this.dataService.save(this.currentFormModel._id, datum).pipe(first()).subscribe(
        (result:any) => {
          console.log(result);
          const { success } = result;

          this.isLoading = false;

          if (!success) {
            console.log("Error when retreiving stats information");
            this.message.error("An error occured. Please, try later");
            return;
          }

          // this.isSaved = true;
          this.modalRef.destroy({success});
        }
      );
    } else {
      this.dataService.update(this.currentData.form, datum).pipe(first()).subscribe(
        (result:any) => {
          console.log(result);
          const { success } = result;

          if (!success) {
            console.log("Error when retreiving stats information");
            this.message.error("An error occured. Please, try later");
            return;
          }

          // this.isSaved = true;
          this.isLoading = false;
          this.modalRef.destroy({success});
          // if (this.router.url.split('/')[0] == 'x') {
          //
          // }
          // else {
          //   this.router.navigate(['/xorms/', this.xormId, 'data']);
          // }

        }
      );
    }


  }

  selectionChangeOnSingle($event, oldValue) {
    console.log($event);
    let source = {
      section: $event.target.id.split('__')[0],
      question: $event.target.id,
      oldValue: oldValue,
      currentValue: undefined
    }
    source["currentValue"] = this.currentFormControls.value[source.section][source.question]; // $event.target.id.split('__')[1]

    console.log(source);
    console.log(this.currentFormModel.form[source.section]);
    console.log(this.currentFormModel.form[source.section].questions[source.question]);

    if ('jumpTo' in this.currentFormModel.form[source.section].questions[source.question]) {
      console.log("IN JUMP TO...");
      console.log(this.currentFormControls.value);
      console.log(source);



      if(source.oldValue && source.oldValue in this.currentFormModel.form[source.section].questions[source.question].jumpTo) {
        let jumpToId = this.currentFormModel.form[source.section].questions[source.question].jumpTo[source.oldValue];
        console.log("OLD VALUE", jumpToId);
        console.log(oldValue);

        // if (jump)
        let toId = {
          section: jumpToId.split('__')[0],
          question: jumpToId
        }

        this._enableElement(source, toId);
      }

      if(source.currentValue && source.currentValue in this.currentFormModel.form[source.section].questions[source.question].jumpTo) {
        let jumpToId = this.currentFormModel.form[source.section].questions[source.question].jumpTo[source.currentValue];
        console.log("CURRENT VALUE", jumpToId);
        let toId = {
          section: jumpToId.split('__')[0],
          question: jumpToId
        }

        this._disableElement(source, toId);
      }
      // if ()
    }
  }

  _disableElement(fromQt, toQt) {
    let allCurrentValues = this.currentFormControls.value;
    let allFromQuestionsKeys = Object.keys(this.currentFormModel.form[fromQt.section].questions); // Object.keys(allCurrentValues[fromQt.section]);
    let fromIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    let j = fromIndex + 1;
    while (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] != toQt.question) {
      // Disable
      if (this.currentFormControls.get(fromQt.section) instanceof FormGroup) {
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).reset();
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).disable();
      }
      j++;
    }

    if (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] == toQt.question) {
      // We reach the targetQuestion
      console.log(this.renderer.selectRootElement(`#${toQt.question}`));
      this.renderer.selectRootElement(`#${toQt.question}`).focus();
      return;
    }

    // Disable all element between the Section
    let allSectionKeys = Object.keys(this.currentFormModel.form);
    j = 1 + allSectionKeys.indexOf(fromQt.section)
    while (j < allSectionKeys.length && allSectionKeys[j] != toQt.section) {
      // Disable all the section
      if (this.currentFormControls.get(allSectionKeys[j]) instanceof FormGroup) {
        this.currentFormControls.get(allSectionKeys[j]).disable();
      }
    }

    // Disable the elements,into the last section, that come before the jumpTo element
    let allToQuestionsKeys = Object.keys(this.currentFormModel.form[toQt.section].questions);
    let toIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    j = 0;
    while (j < toIndex) {
      // Disable
      if (this.currentFormControls.get(toQt.section) instanceof FormGroup) {
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).reset();
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).disable();
      }
      j++;
    }

    this.renderer.selectRootElement(`#${toQt.question}`).focus();
  }

  _enableElement(fromQt, toQt) {
    let allCurrentValues = this.currentFormControls.value;
    console.log(this.currentFormModel.form[fromQt.section]);
    let allFromQuestionsKeys = Object.keys(this.currentFormModel.form[fromQt.section].questions);
    let fromIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    let j = fromIndex + 1;
    console.log(allCurrentValues, allFromQuestionsKeys, fromIndex);
    while (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] != toQt.question) {
      console.log(allFromQuestionsKeys[j]);
      // Enable
      if (this.currentFormControls.get(fromQt.section) instanceof FormGroup) {
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).reset();
        this.currentFormControls.get(fromQt.section).get(allFromQuestionsKeys[j]).enable();
      }
      j++;
    }

    if (j < allFromQuestionsKeys.length && allFromQuestionsKeys[j] == toQt.question) {
      // We reach the targetQuestion
      // this.renderer.selectRootElement(`#${toQt.question}`).focus();
      return;
    }

    // Enable all element between the Section
    let allSectionKeys = Object.keys(this.currentFormModel.form);
    j = 1 + allSectionKeys.indexOf(fromQt.section)
    while (j < allSectionKeys.length && allSectionKeys[j] != toQt.section) {
      // Disable all the section
      if (this.currentFormControls.get(allSectionKeys[j]) instanceof FormGroup) {
        this.currentFormControls.get(allSectionKeys[j]).enable();
      }
    }

    // Enable the elements,into the last section, that come before the jumpTo element
    let allToQuestionsKeys = Object.keys(this.currentFormModel.form[toQt.section].questions);
    let toIndex = allFromQuestionsKeys.indexOf(fromQt.question);
    j = 0;
    while (j < toIndex) {
      // Disable
      if (this.currentFormControls.get(toQt.section) instanceof FormGroup) {
        this.currentFormControls.get(toQt.section).get(allToQuestionsKeys[j]).enable();
      }
      j++;
    }
  }

}
