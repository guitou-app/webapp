import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GridOptions } from 'ag-grid-community';

import { first } from 'rxjs/operators';
import * as _ from "lodash";

import { DataService } from '../../../_services/data.service';

@Component({
  selector: 'app-xorms-data',
  templateUrl: './xorms-data.component.html',
  styleUrls: ['./xorms-data.component.less']
})
export class XormsDataComponent implements OnInit {

  constructor(
    private _location: Location,
  ) { }

  ngOnInit() {
  }

  goBack() {
    this._location.back();
  }

}
