import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XormsComponent } from './xorms.component';

describe('XormsComponent', () => {
  let component: XormsComponent;
  let fixture: ComponentFixture<XormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
