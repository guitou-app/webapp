import { Component, OnInit, ElementRef, Input, SimpleChanges } from '@angular/core';

import * as d3 from 'd3';

import { SocketIoService } from '../../../../_services/socket-io.service';

@Component({
  selector: 'app-boxplot',
  templateUrl: './boxplot.component.html',
  styleUrls: ['./boxplot.component.less']
})
export class BoxplotComponent implements OnInit {

  @Input()
  data:number[];
  @Input()
  idx:string;
  @Input()
  xName:string;
  @Input()
  yName:string;

  data_sorted:number[];
  q1:any;
  median:any;
  q3:any;
  interQuantileRange:any;
  min: any;
  max: any;

  private margin = {top:20, right:20, bottom:20, left:30}
  private width: number;
  private height: number;
  private radius: number;

  private x:any;
  private y:any;
  private yAxis:any;
  private svg:any;
  private g:any;

  constructor(
    private el:ElementRef,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    console.log(this.data);
    // console.log(this.xName);
    // console.log(this.yName);
    this.data = this.data.map((d) => +d);

    this._initSvg();
    this._initAxis();
    this._drawAxis();
    this._drawBox();

    this.socketIoService.onNewDataSaved().subscribe(
      (datum) => {
        var v = datum.values[this.idx.split('__')[0]][this.idx];
        if (+v) {
          this.data.push(+v);
          this._updateBox();
        }
      }
    );
  }

  _initSvg() {
    this.svg = d3.select(this.el.nativeElement).select('svg')
        .attr('width', this.el.nativeElement.parentNode.offsetWidth);

    this.width = +this.svg.attr('width') - this.margin.right - this.margin.left;
    this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;

    this.g = this.svg.append('g')
        .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);


    this.yAxis = this.g.append('g');
  }

  _initAxis() {

    this.data_sorted = this.data.sort(d3.ascending); //((a,b) => a - b); //(d3.ascending);
    // console.log(this.data_sorted);

    this.q1 = d3.quantile(this.data_sorted, .25);
    this.median = d3.quantile(this.data_sorted, .5);
    this.q3 = d3.quantile(this.data_sorted, .75);
    this.interQuantileRange = this.q3 - this.q1;
    this.min = this.q1 - 1.5 * this.interQuantileRange;
    this.max = this.q1 + 1.5 * this.interQuantileRange;

    console.log(this.q1, this.median, this.q3, this.interQuantileRange, this.min, this.max);

    this.y = d3.scaleLinear()
        .domain([(this.min - 10 > 0 ? 0 : this.min - 10) , d3.max(this.data) + 10]) //, (d:any) => +d.length)])
        .range([this.height, 0])
      ;
    console.log(this.y);
  }

  _drawAxis() {
    this.yAxis
        .call(d3.axisLeft(this.y));
  }

  _drawBox() {
    let center = this.width/2;
    let width = this.width/5; // 100;

    // Show the main vertical line
    this.g.append("line")
      .attr("x1", center)
      .attr("x2", center)
      .attr("y1", this.y(this.min))
      .attr("y2", this.y(this.max))
      .attr("stroke", "black")
      .attr('class', 'vertical')
      ;

    // Show the box
    this.g.append('rect')
      .attr("x", center - width/2)
      .attr("y", this.y(this.q3))
      .attr("height", this.y(this.q1) - this.y(this.q3))
      .attr("width", width)
      .attr("stroke", "black")
      .style("fill", "#69b3a2")
      ;

    // Show the medium, min and max horizontal lines
    this.g.selectAll("params")
      .data([this.min, this.median, this.max])
      .enter()
      .append('line')
      .attr("x1", center - width/2)
      .attr("x2", center + width/2)
      .attr("y1", (d) => this.y(+d))
      .attr("y2", (d) => this.y(+d))
      .attr("id", (d, i) => i)
      .attr("stroke", "black")
      .attr('class', 'params')
      // .attr("transform", `translate(0, ${this.margin.bottom})`)
    ;
  }

  _updateBox() {
    this._initAxis();
    console.log(this.q1, this.median, this.q3, this.interQuantileRange, this.min, this.max);
    console.log(this.y(this.median), this.y(this.min), this.y(this.max));

    this.yAxis
        .transition()
        .duration(1000)
        .call(d3.axisLeft(this.y));

    let center = this.width/2;
    let width = this.width/5;

    this.g.selectAll("line.vertical")
      .attr("x1", center)
      .attr("x2", center)
      .attr("y1", this.y(this.min))
      .attr("y2", this.y(this.max))
      .attr("stroke", "black")
      // .attr('class', 'vertical')
      ;

    this.g.selectAll('rect')
      .attr("x", center - width/2)
      .attr("y", this.y(this.q3))
      .attr("height", this.y(this.q1) - this.y(this.q3))
      .attr("width", width)
      .attr("stroke", "black")
      .style("fill", "#69b3a2")
      ;

    let params = this.g.selectAll('line.params')
                    .data([this.min, this.median, this.max]);

    params
        .enter()
        .append('line')
        .merge(params)
        .transition()
        .duration(1000)
        .attr("x1", center - width/2)
        .attr("x2", center + width/2)
        .attr("y1", (d) => this.y(+d))
        .attr("y2", (d) => this.y(+d))
        .attr("id", (d, i) => i)
        .attr("stroke", "black")
        ;
    params
        .exit()
        .remove()
        ;

  }

}
