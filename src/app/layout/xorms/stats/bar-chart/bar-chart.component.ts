import { Component, OnInit, Input, ElementRef, ViewEncapsulation } from '@angular/core';

import * as d3 from 'd3';

import { SocketIoService } from '../../../../_services/socket-io.service';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.sass']
})
export class BarChartComponent implements OnInit {

  @Input()
  data:any[];
  @Input()
  idx:string;

  private margin = {top:20, right:20, bottom:20, left:40}
  private width: number;
  private height: number;
  private radius: number;

  private x:any;
  private y:any;
  private svg:any;
  private g:any;

  private countLabels = 0;

  constructor(
    private el:ElementRef,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.countLabels = this.data.map((d:any) => d.key).filter((v,i, array) => array.indexOf(v) === i).length;

    this._initSvg();
    this._initAxis();
    this._drawAxis();
    this._drawBars();

    this.socketIoService.onNewDataSaved().subscribe(
      (datum) => {
        console.log('socketIoService.onNewDataSaved ', datum);

        var v = datum.values[this.idx.split('__')[0]][this.idx];
        var barIdx = this.data.findIndex(o => o.key == v);
        if (barIdx > -1) {
          if (this.data.hasOwnProperty(barIdx)) {
            // this.data[barIdx].value++;
            this.data[barIdx].value++;
          } else {
            this.data[barIdx].value = 1;
          }
          this._updateBars();
        }

      }
    );
  }

  _initSvg() {
    this.svg = d3.select(this.el.nativeElement).select('svg')
        .attr('width', this.el.nativeElement.parentNode.offsetWidth);

    this.width = +this.svg.attr('width') - this.margin.right - this.margin.left;
    this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;

    this.g = this.svg.append('g')
        .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    console.log("Into _initSvg");
  }

  _initAxis() {
    this.x = d3.scaleBand()
        .rangeRound([0, this.width])
        .padding(0.1)
        .paddingInner(0.05);

    this.y = d3.scaleLinear()
        .rangeRound([this.height, 0]);

    this.x.domain(this.data.map((d:any) => d.key));
    // console.log("_initAxis ... ", d3Array.max(this.data, (d:any) => +d.value))

    this.y.domain([0, d3.max(this.data, (d:any) => +d.value) == 0 ? this.height : d3.max(this.data, (d:any) => +d.value) ]);
    // this.y.domain(this.data.map((d:any) => +d.value));

    console.log("Into _initAxis");
  }

  _drawAxis() {
    this.g.append('g')
        .attr('class', 'axis axis--x')
        .attr('transform', `translate(0, ${this.height})`)
        .call(d3.axisBottom(this.x));

    this.g.append('g')
        .attr('class', 'axis axis--y')
        .call(d3.axisLeft(this.y).ticks(10))
        // .call(d3Axis.axisLeft(this.y).ticks(10, '%'))
        .append('text')
        .attr('class', 'axis-title')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end')
        .text('Frequence')
      ;

    console.log("Into _drawAxis");
  }

  _drawBars() {
    this.g.selectAll('.bar')
        .data(this.data)
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr("fill", "teal")
        .attr("fill", (d) => "rgb(0, 100, " + Math.round(+d.value * 15) + ")")
        .attr('x', (d:any) => this.x(d.key))
        .attr('y', (d:any) => this.y(+d.value))
        .attr('width', this.x.bandwidth())
        .attr('height', (d:any) => this.height - this.y(+d.value));

    this.g.selectAll('.text')
        .data(this.data)
        .enter()
        .append('text')
        .attr('class', 'text')
        .text((d:any) => d.value === 0 ? '' : d.value)
        .attr("x", (d:any) =>  this.x(d.key) + (this.x.bandwidth() / 2))
        .attr("y", (d:any) =>  this.y(+d.value) + 15)
      ;

    console.log("Into _drawBars");
  }

  _updateBars() {
    console.log("_updateBars ", this.data);
    this.y.domain([0, d3.max(this.data, (d:any) => +d.value) == 0 ? this.height : d3.max(this.data, (d:any) => +d.value) ]);

    this.g.select('.axis--y')
      .transition()
      .duration(1000)
      .call(d3.axisLeft(this.y).ticks(10))
    ;

    this.g.selectAll('.bar')
      .data(this.data, this._key)
      .transition()
      .duration(1000)
      .attr('y', (d:any) => this.y(+d.value))
      .attr('height', (d:any) => this.height - this.y(+d.value))
      .attr('fill', (d:any) => "rgb(0, 100, " + Math.round(+d.value * 15) + ")")
    ;

    this.g.selectAll('.text')
      .data(this.data, this._key)
      .transition()
      .duration(1000)
      .attr('class', 'text')
      .text((d:any) => d.value)
      .attr("x", (d:any) =>  this.x(d.key) + (this.x.bandwidth() / 2))
      .attr("y", (d:any) =>  this.y(+d.value) + 15)
    ;
  }

  _key = (d:any) => d.key;

}
