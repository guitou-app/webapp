import { Component, OnInit, Input, ElementRef } from '@angular/core';

import * as d3 from 'd3';

import { SocketIoService } from '../../../../_services/socket-io.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.sass']
})
export class PieChartComponent implements OnInit {

  @Input()
  data:any;
  @Input()
  idx:string;
  @Input()
  xName:string;
  @Input()
  yName:string;

  data_ready:any;

  private margin = {top:20, right:20, bottom:20, left:40}
  private width: number;
  private height: number;
  private radius: number;

  private arc: any;
  private labelArc: any;
  private pie:any;
  private color:any;
  private svg:any;
  private g:any;

  constructor(
    private el:ElementRef,
    private socketIoService: SocketIoService
  ) {

  }

  ngOnInit() {
    this._pieData();
    this._initSvg();
    this._initAxis();
    this._drawPie();

    this.socketIoService.onNewDataSaved().subscribe(
      (datum) => {
        var v = datum.values[this.idx.split('__')[0]][this.idx];
        console.log("V : ", v);
        if (v) {
          if (this.data.hasOwnProperty(v)) {
            this.data[v]++;
          } else {
            this.data[v] = 1;
          }
          this._updatePie();
        }
      }
    );
  }

  _pieData() {
    let d = {};
    for (let i=0; i < this.data.length; i++) {
      let datum = this.data[i];
      d[datum.key] = datum.value;
    }

    this.data = JSON.parse(JSON.stringify(d));
  }

  _initSvg() {
    this.svg = d3.select(this.el.nativeElement).select('svg')
        .attr('width', this.el.nativeElement.parentNode.offsetWidth);

    this.width = +this.svg.attr('width') - this.margin.right - this.margin.left;
    this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;
    this.radius = Math.min(this.width, this.height) / 2;

    this.g = this.svg.append('g')
        .attr('transform', `translate(${this.width/2},${this.height/2})`)
      ;
  }

  _initAxis() {
    this.color = d3.scaleOrdinal()
        .domain(this.data)
        .range(['#98abc5', '#8a89a6', '#7b6888', '#6b486b', '#a05d56', '#d0743c', '#ff8c00']);

    this.arc = d3.arc()
        .outerRadius(this.radius - 10)
        .innerRadius(0);

    this.labelArc = d3.arc()
        .outerRadius(this.radius - 40)
        .innerRadius(0);

    this.pie = d3.pie()
        // .sort(null)
        .value((d:any) => {
          console.log("into pie() ", d);
          return d.value
        });

    this.data_ready = this.pie(d3.entries(this.data));
  }

  _drawPie() {
    let arcs = this.g.selectAll('.arc')
        .data(this.data_ready)
        .enter()
        .append('g')
        .attr('class', 'arc');

    arcs.append('path')
        .attr('d', this.arc)
        .style('fill', (d:any) => this.color(d.data.key));

    arcs.append('text')
        .attr('transform', (d:any) => `translate(${this.labelArc.centroid(d)})`)
        .attr('dy', '.35em')
        .text((d:any) => d.data.value)
  }

  _updatePie() {
    this.pie = d3.pie()
        .sort(null)
        .value((d:any) => d.value);

    this.data_ready = this.pie(d3.entries(this.data));

    let u = this.g.selectAll('path')
        .data(this.data_ready);

    u
      .enter()
      .append('path')
      .merge(u)
      .transition(1000)
      .attr('d', this.arc)
      .style('fill', (d:any) => this.color(d.data.key))
    ;

    u
      .exit()
      .remove()
    ;

    let text = this.g.selectAll('text')
      .data(this.data_ready)
      .text((d:any) => d.data.value)
      ;

  }

}
