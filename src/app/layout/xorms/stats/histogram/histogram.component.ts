import { Component, OnInit, Input, ElementRef, SimpleChanges } from '@angular/core';

import * as d3 from 'd3';

import { SocketIoService } from '../../../../_services/socket-io.service';

@Component({
  selector: 'app-histogram',
  templateUrl: './histogram.component.html',
  styleUrls: ['./histogram.component.sass']
})
export class HistogramComponent implements OnInit {

  @Input()
  data:number[];
  @Input()
  idx:string;
  @Input()
  xName:string;
  @Input()
  yName:string;

  private margin = {top:20, right:20, bottom:20, left:30}
  private width: number;
  private height: number;
  private radius: number;

  private x:any;
  private y:any;
  private xAxis:any;
  private yAxis:any;
  private histogram:any;
  private bins:any;
  private svg:any;
  private g:any;

  constructor(
    private el:ElementRef,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    console.log(this.data);
    // console.log(this.xName);
    // console.log(this.yName);

    this._initSvg();
    this._initAxis();
    this._drawAxis();
    this._drawBars();

    this.socketIoService.onNewDataSaved().subscribe(
      (datum) => {
        var v = datum.values[this.idx.split('__')[0]][this.idx];
        if (+v) {
          this.data.push(+v);
          this._updateBars();
        }
        // var barIdx = this.data.findIndex(o => o.key == v);
        // if (barIdx > -1) {
        //   this.data[barIdx].value++;
        //   this._updateBars();
        // }

      }
    );
  }

  _initSvg() {
    console.log("_initSvg...");
    this.svg = d3.select(this.el.nativeElement).select('svg')
        .attr('width', this.el.nativeElement.parentNode.offsetWidth);

    this.width = +this.svg.attr('width') - this.margin.right - this.margin.left;
    this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;

    this.g = this.svg.append('g')
        .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    this.yAxis = this.g.append('g');
    this.xAxis = this.g.append('g');
  }

  _initAxis() {
    this.x = d3.scaleLinear()
        .domain([0, d3.max(this.data)])
        .range([0, this.width]);

    this.histogram = d3.histogram()
        .value((d) => +d)
        .domain(this.x.domain())
        .thresholds(this.x.ticks(5))
      ;

    this.bins = this.histogram(this.data);
    this.y = d3.scaleLinear()
        .domain([0, d3.max(this.bins, (d:any) => +d.length)])
        .range([this.height, 0])
  }

  _drawAxis() {
    // this.g.append('g')
    this.xAxis // = this.g.append('g')
        .attr('transform', `translate(0, ${this.height})`) // - this.margin.bottom})`)
        .call(d3.axisBottom(this.x));

    // this.g.append('g')
    this.yAxis
        .call(d3.axisLeft(this.y));
  }

  _drawBars() {
    this.g
      .selectAll('rect')
        .data(this.bins)
        .enter()
        .append('rect')
        .attr("x", (d:any) => 1) //this.x(d.x0) + 1)
        .attr("transform", (d:any) => `translate(${this.x(d.x0)}, ${this.y(d.length)})`)
        // .attr("y", d => this.y(d.length))
        .attr("width", (d:any) => Math.max(0, this.x(d.x1) - this.x(d.x0) - 1)) // this.x(d.x1) - this.x(d.x0) - 1) //
        .attr("height", (d:any) => this.height - this.y(d.length)) // this.y(0) - this.y(d.length));
        .attr("fill", "steelblue")
  }

  _updateBars() {
    console.log("_updateBars ", this.data);
    this.x.domain([0, d3.max(this.data)]);
    this.xAxis
        .transition()
        .duration(1000)
        .call(d3.axisBottom(this.x));

    this.histogram = d3.histogram()
        .value((d) => +d)
        .domain(this.x.domain())
        .thresholds(this.x.ticks(5))
      ;

    this.bins = this.histogram(this.data);
    this.y.domain([0, d3.max(this.bins, (d:any) => +d.length)])
    this.yAxis
        .transition()
        .duration(1000)
        .call(d3.axisLeft(this.y));

    let bars = this.g.selectAll('rect')
                    .data(this.bins);

    bars
        .enter()
        .append('rect')
        .merge(bars)
        .transition()
        .duration(1000)
          .attr('x', 1)
          .attr('transform', (d) => `translate(${this.x(d.x0)}, ${this.y(d.length)})`)
          .attr("width", (d:any) => Math.max(0, this.x(d.x1) - this.x(d.x0) - 1)) // this.x(d.x1) - this.x(d.x0) - 1) //
          .attr("height", (d:any) => this.height - this.y(d.length)) // this.y(0) - this.y(d.length));
        ;
    bars
        .exit()
        .remove()
        ;
  }

}
