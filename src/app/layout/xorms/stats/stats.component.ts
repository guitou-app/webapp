import { Component, OnInit, Inject, ViewEncapsulation, Input, TemplateRef} from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';

import { first } from 'rxjs/operators';
import * as _ from "lodash";
import { Analysis } from 'src/app/_models/analysis';
import { AnalysisService } from '../../../_services/analysis.service';
import { XormsService } from '../../../_services/xorms.service';
import { SocketIoService } from '../../../_services/socket-io.service';

@Component({
  selector: 'app-dashboards',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class StatsComponent implements OnInit {

  projectId: string;
  xormId: string;
  dashboards:any = {};
  groupedDashbaords:any;
  active:number;
  analysis:Analysis[] = [];

  constructor(
    private activatedRoute:ActivatedRoute,
    private modalService: NzModalService,
    private _location: Location,
    private analysisService: AnalysisService,
    private socketIoService: SocketIoService
  ) { }

  ngOnInit() {
    // this.projectId = this.activatedRoute.snapshot.params['projectId'];
    this.projectId = this.activatedRoute.snapshot.parent.params['projectId']; // this.activatedRoute.parent.parent.parent.snapshot.params['projectId'];
    this.xormId = this.activatedRoute.snapshot.parent.params['xormId']; // this.activatedRoute.parent.parent.parent.snapshot.params['projectId'];
    console.log('OnINIT ...', this.projectId);

    if (this.projectId) {
      this._loadDashbaords();

      console.log("Before socketIoService.joinDashbaords ..", this.projectId);
      this.socketIoService.joinDashbaords(this.projectId).pipe(first()).subscribe(
        (result:any) => {
          console.log("Into socketIoService.joinDashbaords");
          console.log(result);
        }
      );
    }

    this.socketIoService.onNewDataSaved().pipe(first()).subscribe(
      (result:any) => {
        console.log("Into socketIoService.onNewDataSaved");
        console.log(result);
      }
    );

  }

  _loadDashbaords() {
    this.analysisService.fetchDashbaords(this.projectId).pipe(first()).subscribe(
      (result:Analysis[]) => {
        console.log(result);
        this.analysis = result;
      }
    )
  }

  goBack() {
    this._location.back();
  }

  onCreateNewChartClicked() {
    let modal = this.modalService.create({
      nzTitle: 'Create a new chart',
      nzContent: NewChartDialog,
      nzComponentParams: {
        projectId: this.projectId
      },
    });

    modal.afterClose.subscribe(result => {
      // this.router.navigate(['/xorms/', result.xorm._id]);
      console.log('The dialog was closed');
      console.log(result);

      this.analysis.push(result);
      this.analysis = JSON.parse(JSON.stringify(this.analysis));
    }); 
  }

  onDeleteChart(chart:Analysis) {

    this.analysisService.deleteChart(this.projectId, chart.id).pipe(first()).subscribe(
      (_:boolean) => {
        this.analysis = this.analysis.filter(c => c.id != chart.id);
      },
      (err) => {

      }
    )
  }

  onChartChoiceClicked(selected) {
    this.active = selected;
  }

  onDashbaordsInitClicked() {
    console.log("onDashbaordsInitClicked...");
    // No Good. Get the select Xorm
    this.analysisService.initChart(this.projectId, this.projectId).pipe(first()).subscribe(
      (result:any) => {
        console.log(result);

        if (!result.success) {
          console.log("Error when retreiving dashboards information");

          return;
        }

        if (result.data != null) {
          this.dashboards = JSON.parse(JSON.stringify(result.data));
          this.analysis = JSON.parse(JSON.stringify(this.dashboards.analysis));
        }
      }
    )
  }

  onStatsInitClicked() {
    console.log("onStatsInitClicked()");
  }

}

@Component({
  selector: 'app-new-chart-dialog',
  templateUrl: 'new-chart-dialog.html',
})
export class NewChartDialog implements OnInit {
  @Input() projectId: string;

  active:any;
  currentXorm:any;

  selectedVariable:any = undefined;
  selectedChart:any = undefined;
  stats:any[] = [];
  extendedVariables:any[] = [];

  constructor(
    private analysisService: AnalysisService,
    private xormsService: XormsService,
    private socketIoService: SocketIoService,
    private modal: NzModalRef,
  ) {}

  ngOnInit() {
    this.xormsService.fetchOne(this.projectId).pipe(first()).subscribe(
      (result:any) => {
        console.log(result);

        if (!result.success) {
          console.log("Error when retreiving dashboards information");

          return;
        }

        this.currentXorm = result.data;
        this.buildVariables();
      }
    )
  }

  buildVariables() {
    this.currentXorm.forms.forEach(form => {
      console.log(form);
      let questions = [];
      Object.keys(form.form).forEach(section => {
        questions = {...questions, ...form.form[section].questions};
      });

      this.extendedVariables.push({
        ...form,
        questions
      })
    });
    console.log(this.extendedVariables);
  }

  onNoClick(): void {
    this.modal.destroy();
  }

  onVariableSelected() {
    // console.log("Variable...", variable, this.selectedVariable);
    // this.selectedVariable = variable;
    console.log(this.selectedChart);
    console.log(this.selectedVariable);
    if (this.selectedVariable != undefined && this.selectedChart != undefined) {
      this._getDataChart();
    }
  }

  onChartChoiceClicked(selected) {
    this.selectedChart = selected;
    console.log(this.selectedChart);
    console.log(this.selectedVariable);
    if (this.selectedVariable != undefined && this.selectedChart != undefined) {
      this._getDataChart();
    }
  }

  _getDataChart() {
    this.socketIoService.emitCreateChartData(this.projectId, this.selectedVariable, this.selectedChart).subscribe(
      (data) => {
        console.log(data);

        this.stats = data;
      }
    );
  }

  onOkClick() {
    console.log(this.selectedVariable);
    console.log(this.selectedChart);

    let [xormId, varid] = this.selectedVariable.split(" xxx ");
    let [section] = varid.split("__");
    
    const question = this.currentXorm.forms.find(f => f._id == xormId).form[section].questions[varid];
    let params = {
      variable: varid,
      question,
      xormId,
      stats: this.stats,
      chart: this.selectedChart
    }

    let analysis: Analysis = {
      title: "analysis xxxx",
      projectId: this.projectId,
      xormId,
      type: this.selectedChart, 
      [this.selectedChart]: {
        variable: varid,
        data: this.stats,
        options: {
          bins: 5
        }
      }
    }
    this.analysisService.addChart(this.projectId, analysis).pipe(first()).subscribe(
      (analysis:Analysis) => {
        console.log(analysis);

        this.modal.destroy(analysis);
      }, 
      (err) => {
        
      }
    )
  }

}
