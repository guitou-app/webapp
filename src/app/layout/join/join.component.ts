import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Collaborator } from 'src/app/_models/collaborator';
import { AuthService } from 'src/app/_services/auth.service';
import { LocalUserService } from 'src/app/_services/local-user.service';
import { XormsService } from 'src/app/_services/xorms.service';

import { first } from 'rxjs/operators';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./join.component.less']
})
export class JoinComponent implements OnInit {

  invitationId: string = undefined;
  projectId: string = undefined;

  isLoading: boolean = false;
  done: boolean = false;
  errorOccurred: boolean = false;
  errorMessage: string = '';

  invitation: Invitation;
  collaborator: Collaborator;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private localUserService: LocalUserService,
    private xormsService: XormsService
  ) { }

  ngOnInit(): void {
    this.projectId = this.activatedRoute.snapshot.params['projectId'];
    this.invitationId = this.activatedRoute.snapshot.params['invitationId'];
    
    this.checkInvitation();
  }

  private checkInvitation() {
    this.isLoading = true;
    this.xormsService.checkInvitation(this.projectId, this.invitationId).subscribe(
      (result: Invitation) => {
        this.isLoading = false;
        // this.collaborator = result;
        this.invitation = result;
        this.collaborator = this.invitation.collaborator;
        // this.isInvitationUser()
        if (this.collaborator.status == 'accepted') {
          this.router.navigate(['/xorms/', this.projectId]);
        }
      },
      (error) => {
        this.isLoading = false;

        this.done = true;
        this.errorOccurred = true;
        this.errorMessage = error;
      }
    )
  }

  isUserConnectedMatched(): boolean {
    return this.collaborator 
      ? this.collaborator.email == this.localUserService.user.email 
      : false;
  }

  handleAnswer(answer:string) {
    this.isLoading = true;

    this.xormsService.answerInvitation(this.projectId, this.invitationId, answer).subscribe(
      (result: Collaborator) => {
        this.isLoading = false;
        
        if (this.collaborator.status == 'accepted') {
          this.router.navigate(['/xorms/', this.projectId]);
        } else {
          this.router.navigate(['']);
        }
      },
      (error) => {
        this.isLoading = false;

        this.done = true;
        this.errorOccurred = true;
        this.errorMessage = error;
      }
    )
  }

  signIn() {
    this.isLoading = true;

    this.authService.signOut().pipe(first()).subscribe(
			(result:any) => {
      	localStorage.removeItem('token');
      	localStorage.removeItem('refresh-token');
        localStorage.removeItem('current-customer');
        
        this.isLoading = false;
        console.log(this.router.url, this.collaborator);
        this.router.navigate(['/auth/sign-in/'], {
          queryParams: {
            return: this.router.url,
            email: this.collaborator.email
          }
        });
			},
			(error) => {
        this.isLoading = false;
        
        console.log("An error occured. Restart please.");
				console.log(error);
			}
		);
  }

  signUp() {
    this.isLoading = true;

    this.authService.signOut().pipe(first()).subscribe(
			(result:any) => {
      	localStorage.removeItem('token');
      	localStorage.removeItem('refresh-token');
      	localStorage.removeItem('current-customer');

        console.log(localStorage.getItem("token"));
        this.isLoading = false;
        this.router.navigate(['/auth/sign-up'], {
          queryParams: {
            return: this.router.url,
            email: this.collaborator.email
          }
        });
			},
			(error) => {
        this.isLoading = false;
        // this.error = "An error occured. Restart please.";
        console.log("An error occured. Restart please.");
				console.log(error);
			}
		);
  }

}

interface Invitation {
  project: {
    title: string;
    description: string;
  };
  collaborator: Collaborator,
  email: string
}