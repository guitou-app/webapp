import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// import { NgZorroAntdModule, NzConfig, NZ_CONFIG } from 'ng-zorro-antd';
import {  NzConfig, NZ_CONFIG } from 'ng-zorro-antd/core/config';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzMentionModule } from 'ng-zorro-antd/mention';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzTransferModule } from 'ng-zorro-antd/transfer';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzCalendarModule } from 'ng-zorro-antd/calendar';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';







import { CdkTableModule } from '@angular/cdk/table';
import { DragDropModule } from '@angular/cdk/drag-drop';
// import {
//     MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule,
//     MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
//     MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule,
//     MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule,
//     MatToolbarModule, MatTooltipModule
// } from '@angular/material';

import { NgPipesModule } from 'ngx-pipes';
import { AgGridModule } from 'ag-grid-angular';
import { NgSelectModule } from '@ng-select/ng-select';

import { NavbarComponent } from './navbar/navbar.component';

const ngZorroConfig: NzConfig = {
  message: { nzTop: 120 },
  notification: { nzTop: 240 }
};

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    // BrowserAnimationsModule,

    // TabsModule.forRoot(),
    // ModalModule.forRoot(),
    // TooltipModule.forRoot(),

    NgPipesModule,
    AgGridModule.withComponents([]),
    NgSelectModule,

    CdkTableModule,
    DragDropModule,
    // MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule,
    // MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
    // MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule,
    // MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule,
    // MatToolbarModule, MatTooltipModule,

    // NgZorroAntdModule
    NzSpaceModule,
    NzButtonModule, NzIconModule, NzTypographyModule, NzDividerModule, NzLayoutModule,
    NzAffixModule, NzBreadCrumbModule, NzDropDownModule, NzMenuModule, NzPageHeaderModule,
    NzPaginationModule, NzStepsModule,
    NzAutocompleteModule, NzCheckboxModule, NzDatePickerModule, NzFormModule, NzInputModule,
    NzInputNumberModule, NzMentionModule, NzRadioModule, NzRateModule, NzSelectModule,
    NzSliderModule, NzSwitchModule, NzTimePickerModule, NzTransferModule, NzUploadModule,
    NzAlertModule, NzDrawerModule, NzMessageModule, NzModalModule, NzNotificationModule,
    NzPopconfirmModule, NzProgressModule, NzResultModule, NzSkeletonModule, NzSpinModule,
    NzAvatarModule, NzBadgeModule, NzCalendarModule, NzCardModule, NzCarouselModule,
    NzCollapseModule, NzCommentModule, NzDescriptionsModule, NzEmptyModule, NzImageModule,
    NzListModule, NzPopoverModule, NzStatisticModule, NzTableModule, NzTabsModule, 
    NzTagModule, NzTimelineModule, NzToolTipModule
  ],
  providers: [
    { provide: NZ_CONFIG, useValue: ngZorroConfig }
  ],
  exports: [
    NavbarComponent,

    // TabsModule,
    // ModalModule,
    // TooltipModule,
    // BrowserAnimationsModule,

    NgPipesModule,
    AgGridModule,
    NgSelectModule,

    CdkTableModule,
    DragDropModule,
    // MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule,
    // MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
    // MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule,
    // MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule,
    // MatToolbarModule, MatTooltipModule,

    NzSpaceModule,
    NzButtonModule, NzIconModule, NzTypographyModule, NzDividerModule, NzLayoutModule,
    NzAffixModule, NzBreadCrumbModule, NzDropDownModule, NzMenuModule, NzPageHeaderModule,
    NzPaginationModule, NzStepsModule,
    NzAutocompleteModule, NzCheckboxModule, NzDatePickerModule, NzFormModule, NzInputModule,
    NzInputNumberModule, NzMentionModule, NzRadioModule, NzRateModule, NzSelectModule,
    NzSliderModule, NzSwitchModule, NzTimePickerModule, NzTransferModule, NzUploadModule,
    NzAlertModule, NzDrawerModule, NzMessageModule, NzModalModule, NzNotificationModule,
    NzPopconfirmModule, NzProgressModule, NzResultModule, NzSkeletonModule, NzSpinModule,
    NzAvatarModule, NzBadgeModule, NzCalendarModule, NzCardModule, NzCarouselModule,
    NzCollapseModule, NzCommentModule, NzDescriptionsModule, NzEmptyModule, NzImageModule,
    NzListModule, NzPopoverModule, NzStatisticModule, NzTableModule, NzTabsModule, 
    NzTagModule, NzTimelineModule, NzToolTipModule
  ]
})
export class SharedModule { }
