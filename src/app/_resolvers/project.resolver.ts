import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { Project } from "../_models/project";
import { XormsService } from "../_services/xorms.service";

@Injectable({ providedIn: 'root' })
export class ProjectResolver implements Resolve<Project> {

  constructor (private service: XormsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Project | Observable<Project> | Promise<Project> {
    console.log('projeect resolvers is called ...');
    return this.service.fetchOne(route.paramMap.get('projectId'));
    // .pipe(catchError(() => {
    //   return of('project not available at this time');
    // }));
  }
}
