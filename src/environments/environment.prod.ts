export const environment = {
  production: true,
  sockeIoUrl: 'https://api.guitou.cm',
  apiUrl: 'https://api.guitou.cm/api',
  websiteUrl: 'https://www.guitou.cm'
};
