export const environment = {
  production: true,
  environment: 'STAGING',
  sockeIoUrl: 'https://api.guitou.local',
  apiUrl: 'https://api.guitou.local/api',
  websiteUrl: 'https://www.guitou.local'
};
