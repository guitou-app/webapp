FROM node:14-alpine As builder

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --silent

COPY . .

RUN npm run prod

FROM nginx:1.15.8-alpine

COPY --from=builder /usr/src/app/dist/guitou-web/ /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf
